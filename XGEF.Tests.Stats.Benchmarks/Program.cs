﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using XGEF.Tests.Stats.Benchmarking;

namespace StatBenchmark
{
	class Program
	{
		static void Main(string[] args)
		{
			List<BenchmarkAction> benchmarks = new List<BenchmarkAction>()
			{
				DecimalStatBenchmarks.Decimal_Add,
				DecimalStatBenchmarks.DecimalStat_Add,
				DecimalStatBenchmarks.Decimal_Subtract,
				DecimalStatBenchmarks.DecimalStat_Subtract,
				DecimalStatBenchmarks.Decimal_Multiply,
				DecimalStatBenchmarks.DecimalStat_Multiply,
				DecimalStatBenchmarks.Decimal_Divide,
				DecimalStatBenchmarks.DecimalStat_Divide,
				DecimalStatBenchmarks.Decimal_Modulo,
				DecimalStatBenchmarks.DecimalStat_Modulo,
				DoubleStatBenchmarks.Double_Add,
				DoubleStatBenchmarks.DoubleStat_Add,
				DoubleStatBenchmarks.Double_Subtract,
				DoubleStatBenchmarks.DoubleStat_Subtract,
				DoubleStatBenchmarks.Double_Multiply,
				DoubleStatBenchmarks.DoubleStat_Multiply,
				DoubleStatBenchmarks.Double_Divide,
				DoubleStatBenchmarks.DoubleStat_Divide,
				DoubleStatBenchmarks.Double_Modulo,
				DoubleStatBenchmarks.DoubleStat_Modulo,
				FloatStatBenchmarks.Single_Add,
				FloatStatBenchmarks.FloatStat_Add,
				FloatStatBenchmarks.Single_Subtract,
				FloatStatBenchmarks.FloatStat_Subtract,
				FloatStatBenchmarks.Single_Multiply,
				FloatStatBenchmarks.FloatStat_Multiply,
				FloatStatBenchmarks.Single_Divide,
				FloatStatBenchmarks.FloatStat_Divide,
				FloatStatBenchmarks.Single_Modulo,
				FloatStatBenchmarks.FloatStat_Modulo,
				ByteStatBenchmarks.Byte_Add,
				ByteStatBenchmarks.ByteStat_Add,
				ByteStatBenchmarks.Byte_Subtract,
				ByteStatBenchmarks.ByteStat_Subtract,
				ByteStatBenchmarks.Byte_Multiply,
				ByteStatBenchmarks.ByteStat_Multiply,
				ByteStatBenchmarks.Byte_Divide,
				ByteStatBenchmarks.ByteStat_Divide,
				ByteStatBenchmarks.Byte_Modulo,
				ByteStatBenchmarks.ByteStat_Modulo,
				SByteStatBenchmarks.SByte_Add,
				SByteStatBenchmarks.SByteStat_Add,
				SByteStatBenchmarks.SByte_Subtract,
				SByteStatBenchmarks.SByteStat_Subtract,
				SByteStatBenchmarks.SByte_Multiply,
				SByteStatBenchmarks.SByteStat_Multiply,
				SByteStatBenchmarks.SByte_Divide,
				SByteStatBenchmarks.SByteStat_Divide,
				SByteStatBenchmarks.SByte_Modulo,
				SByteStatBenchmarks.SByteStat_Modulo,
				ShortStatBenchmarks.Int16_Add,
				ShortStatBenchmarks.ShortStat_Add,
				ShortStatBenchmarks.Int16_Subtract,
				ShortStatBenchmarks.ShortStat_Subtract,
				ShortStatBenchmarks.Int16_Multiply,
				ShortStatBenchmarks.ShortStat_Multiply,
				ShortStatBenchmarks.Int16_Divide,
				ShortStatBenchmarks.ShortStat_Divide,
				ShortStatBenchmarks.Int16_Modulo,
				ShortStatBenchmarks.ShortStat_Modulo,
				UShortStatBenchmarks.UInt16_Add,
				UShortStatBenchmarks.UShortStat_Add,
				UShortStatBenchmarks.UInt16_Subtract,
				UShortStatBenchmarks.UShortStat_Subtract,
				UShortStatBenchmarks.UInt16_Multiply,
				UShortStatBenchmarks.UShortStat_Multiply,
				UShortStatBenchmarks.UInt16_Divide,
				UShortStatBenchmarks.UShortStat_Divide,
				UShortStatBenchmarks.UInt16_Modulo,
				UShortStatBenchmarks.UShortStat_Modulo,
				IntStatBenchmarks.Int32_Add,
				IntStatBenchmarks.IntStat_Add,
				IntStatBenchmarks.Int32_Subtract,
				IntStatBenchmarks.IntStat_Subtract,
				IntStatBenchmarks.Int32_Multiply,
				IntStatBenchmarks.IntStat_Multiply,
				IntStatBenchmarks.Int32_Divide,
				IntStatBenchmarks.IntStat_Divide,
				IntStatBenchmarks.Int32_Modulo,
				IntStatBenchmarks.IntStat_Modulo,
				UIntStatBenchmarks.UInt32_Add,
				UIntStatBenchmarks.UIntStat_Add,
				UIntStatBenchmarks.UInt32_Subtract,
				UIntStatBenchmarks.UIntStat_Subtract,
				UIntStatBenchmarks.UInt32_Multiply,
				UIntStatBenchmarks.UIntStat_Multiply,
				UIntStatBenchmarks.UInt32_Divide,
				UIntStatBenchmarks.UIntStat_Divide,
				UIntStatBenchmarks.UInt32_Modulo,
				UIntStatBenchmarks.UIntStat_Modulo,
				LongStatBenchmarks.Int64_Add,
				LongStatBenchmarks.LongStat_Add,
				LongStatBenchmarks.Int64_Subtract,
				LongStatBenchmarks.LongStat_Subtract,
				LongStatBenchmarks.Int64_Multiply,
				LongStatBenchmarks.LongStat_Multiply,
				LongStatBenchmarks.Int64_Divide,
				LongStatBenchmarks.LongStat_Divide,
				LongStatBenchmarks.Int64_Modulo,
				LongStatBenchmarks.LongStat_Modulo,
				ULongStatBenchmarks.UInt64_Add,
				ULongStatBenchmarks.ULongStat_Add,
				ULongStatBenchmarks.UInt64_Subtract,
				ULongStatBenchmarks.ULongStat_Subtract,
				ULongStatBenchmarks.UInt64_Multiply,
				ULongStatBenchmarks.ULongStat_Multiply,
				ULongStatBenchmarks.UInt64_Divide,
				ULongStatBenchmarks.ULongStat_Divide,
				ULongStatBenchmarks.UInt64_Modulo,
				ULongStatBenchmarks.ULongStat_Modulo,

			};

			foreach (var func in benchmarks)
			{
				Profile(1000000, func);
			}

			Console.ReadLine();
		}

		public delegate double BenchmarkAction(int count, Stopwatch watch);
		public static double Profile(int iterations, BenchmarkAction func)
		{
			//Run at highest priority to minimize fluctuations caused by other processes/threads
			Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
			Thread.CurrentThread.Priority = ThreadPriority.Highest;

			var watch = new Stopwatch();
			// warm up 
			func(10, watch);

			watch = new Stopwatch();

			// clean up
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			double result = func(iterations, watch);

			Console.WriteLine("{0}\t{1}\t{2}", func.Method.Name, result / iterations, result);
			return result;
		}
	}


}
