﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Compiler
{
	public enum MemberType
	{
		Using,
		Namespace,
		Class,
		Struct,
		Interface,
		Enum,
		Delegate,
		Event,
		Method,
		Constructor,
		Finalizer,
		Operator,
		Constant,
		Field,
		Property,
		Indexer,
		Attribute
	}
}
