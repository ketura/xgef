﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF
{
	public interface ISystem
	{
		string Name { get; }

		void LoadSettings(XGEFSettings settings);

		void PreInit();
		void Init();
		void PostInit();

		void PreProcess();
		void Process();
		void PostProcess();

		bool Initialized { get; }
		SystemManager Manager { get; set; }
	}

	public interface IModSystem : ISystem
	{

	}

	//Basically a system that is never modded, doesn't define game asset types, and doesn't directly interact with the game.
	public abstract class Utility : ISystem
	{
		public string Name { get; protected set; }

		public SystemManager Manager { get; set; }

		public bool Initialized { get; protected set; }

		public Utility()
		{
			Name = this.GetType().Name;
		}

		public virtual void Init()
		{
			if (Initialized)
				throw new InvalidOperationException($"Cannot initialize {Name}!  It has already been initialized!");

			Initialized = true;
		}
		public virtual void Process() { }

		public override string ToString()
		{
			return Name;
		}

		public virtual void LoadSettings(XGEFSettings settings) { }
		public virtual void PreInit() { }
		public virtual void PostInit() { }
		public virtual void PreProcess() { }
		public virtual void PostProcess() { }
	}

	//The primary means through which a game is defined.
	public abstract class System : ISystem
	{
		public virtual void LoadSettings(XGEFSettings settings) { }

		public virtual void PreInit() { }
		public abstract void Init();
		public virtual void PostInit() { }

		public virtual void PreProcess() { }
		public abstract void Process();
		public virtual void PostProcess() { }

		public bool Initialized { get; protected set; }
		public string Name { get; protected set; }
		public SystemManager Manager { get; set; }
		//public virtual IEnumerable<EntityDefinition> EntityDefinitions { get; protected set; }

		public System() : base()
		{
			Name = this.GetType().Name;
			//EntityDefinitions = new List<EntityDefinition>();
		}

		public override string ToString()
		{
			return Name;
		}
	}

	//One-half of a moddable system, this is the half that will be packed into the game files as regular code and compiled as needed.
	public abstract class ModSystem : System
	{
		public CoreSystem CoreSystemBase { get; protected set; }
		public virtual Type CoreSystemType { get; protected set; }

		public virtual void SetCoreSystem(CoreSystem system)
		{
			CoreSystemBase = system;
			CoreSystemType = system.GetType();
			Manager = system.Manager;
		}

		public virtual void SetCoreSystem(object system)
		{
			SetCoreSystem(system as CoreSystem);
		}

		public override void Init()
		{
			if (CoreSystemBase == null)
				throw new InvalidOperationException("Cannot initialize ModSystem!  Its compiled half was never provided!");
		}

		public void Pair(CoreSystem system)
		{
			SetCoreSystem(system);
			system.SetModdedSystem(this);
		}

	}

	public abstract class ModSystem<T> : ModSystem
		where T : CoreSystem
	{
		public T CoreSystem { get { return CoreSystemBase as T; } }
		public override Type CoreSystemType { get { return typeof(T); } }

		public virtual void SetCoreSystem(T system)
		{
			SetCoreSystem(system as CoreSystem);
		}
	}

	//A system that is intended to be modded.  This class should probably not ever be inherited from
	// directly, but it is available due to not being able to easily instantiate generic types that
	// are not known at compile time.
	public abstract class CoreSystem : System
	{
		public ModSystem ModSystemBase { get; protected set; }
		public virtual Type ModdedSystemType { get; protected set; }

		public bool AutomaticallyPair { get; protected set; } = true;

		public string ModdedSystemPath { get; protected set; }
		public string ModdedSystemName { get; protected set; }

		public virtual void SetModdedSystem(ModSystem moddedSystem)
		{
			if (!ModdedSystemType.IsAssignableFrom(moddedSystem.GetType()))
				throw new InvalidOperationException($"Cannot accept {moddedSystem.Name} for {Name} as it is not of type {ModdedSystemType.Name}.");

			ModSystemBase = moddedSystem;
			ModdedSystemType = moddedSystem.GetType();
		}

		public virtual void SetModdedSystem(object moddedSystem)
		{
			SetModdedSystem(moddedSystem as ModSystem);
		}

		public override void PreInit()
		{
			if (ModSystemBase != null)
				ModSystemBase.PreInit();
		}

		public override void Init()
		{
			if (ModSystemBase == null)
			{
				if(!AutomaticallyPair)
				{
					Manager.Warn($"System {Name} is set to manual pair but no pair was ever provided by the user.  This system will be stripped from the game.");
					//TODO: actually remove this system from the manager.
					//Manager[Name] = null;
					return;
				}
				else
				{
					throw new InvalidOperationException($"Cannot initialize ModdableCoreSystem {Name}!  Its modded half was never provided!");
				}
			}

			ModSystemBase.Init();

			Initialized = true;
		}

		public override void PostInit()
		{
			if (ModSystemBase != null)
				ModSystemBase.PostInit();
		}

		public void Pair(ModSystem system)
		{
			SetModdedSystem(system);
			system.SetCoreSystem(this);
		}

		public override void PreProcess()
		{
			if(Initialized)
			{
				ModSystemBase.PreProcess();
			}
		}

		public override void Process()
		{
			if (Initialized)
			{
				ModSystemBase.Process();
			}
		}

		public override void PostProcess()
		{
			if (Initialized)
			{
				ModSystemBase.PostProcess();
			}
		}
	}

	public abstract class CoreSystem<T> : CoreSystem
		where T : ModSystem
	{
		public override Type ModdedSystemType { get { return typeof(T); } }
		public T ModSystem { get { return ModSystemBase as T; } }

		public CoreSystem() : base() { }

		public virtual void SetModdedSystem(T moddedSystem)
		{
			ModSystemBase = moddedSystem;
		}



		//public Dictionary<string, IEvent> Events { get; protected set; }


		//public virtual void RegisterForEvent(string eventName, Priority priority, Delegate callback) { }

		//public virtual void UnregisterForEvent(string eventName, Delegate callback) { }

		//public ModdableSystem()
		//{
		//	//Events = new Dictionary<string, IEvent>();
		//}

		//public void AddEvent(string name, EventArgs args, Type argsType = null)
		//{
		//	if (Events.ContainsKey(name))
		//		return;
		//}

	}

	//GeneratesEvent
	//AppendEvent
	//PrependEvent
	//OverrideClass
	//OverrideFunction
	//AppendFunction
	//PrependFunction
	//DeleteFunction

	//Don't Serialize attribute
	//naked code with no class wrapped in a class named after the file path?
}
