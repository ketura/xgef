﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Util
{
	public static class StringHash
	{
		static StringHash()
		{

		}

		public static int XHash(string name)
		{
			return name.GetHashCode();
		}

		public static int XHash(object obj)
		{
			return XHash(obj.ToString());
		}
	}
}
