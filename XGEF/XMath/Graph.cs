﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.XMath
{
	public class Graph<T>
	{
		public List<Node<T>> Nodes { get; protected set; }

		public void Add(Node<T> node)
		{
			Nodes.Add(node);
		}

		public Node<T> Add(T value)
		{
			var node = new Node<T>(value);
			Nodes.Add(node);
			return node;
		}

		public bool Remove(T value)
		{
			var node = Nodes.FirstOrDefault(x => x.Value.Equals(value));
			if (node == null)
				return false;
			return Remove(node);
		}

		public bool Remove(Node<T> node)
		{
			if (!Nodes.Contains(node))
				return false;

			Nodes.Remove(node);

			foreach (var other in Nodes)
			{
				other.Remove(node);
			}

			return true;
		}

		public void AddDirEdge(Node<T> from, T to) { from.Add(to); }
		public void AddDirEdge(Node<T> from, Node<T> to) { from.Add(to); }
		public void AddDirEdge(T from, Node<T> to) { AddDirEdge(from, to.Value); }
		public void AddDirEdge(T from, T to)
		{
			var node = GetNode(from);
			if (node != null && this.Contains(to))
				node.Add(to);
		}

		public bool Contains(Node<T> node)
		{
			return Nodes.Contains(node);
		}

		public bool Contains(T value)
		{
			return Nodes.Any(x => x.Value.Equals(value));
		}

		public int Count { get { return Nodes.Count; } }

		public Node<T> GetNode(T value)
		{
			return Nodes.FirstOrDefault(x => x.Value.Equals(value));
		}

		public IEnumerable<Node<T>> GetStartNodes()
		{
			List<Node<T>> list = Nodes.ToList();

			foreach (var node in Nodes)
			{
				list.RemoveAll(x => node.Contains(x));
			}

			return list;
		}

		public IEnumerable<Node<T>> GetEndNodes()
		{
			return Nodes.Where(x => x.Edges.Count == 0).ToList();
		}

		public IEnumerable<Node<T>> GetUnmarkedNodes()
		{
			return Nodes.Where(x => !x.Visited).ToList();
		}

		public void UnmarkAll()
		{
			foreach (var node in Nodes)
			{
				node.Visited = false;
				node.Index = null;
				node.Lowlink = 0;
			}
		}


		public Graph() : this(null) { }
		public Graph(IEnumerable<T> data)
		{
			Nodes = new List<Node<T>>();
			if (data != null)
			{
				Nodes.AddRange(data.Select(x => new Node<T>(x)));
			}
		}
	}

	public class Node<T> : IEnumerable<T>
	{
		public bool Visited { get; set; }
		public int? Index { get; set; }
		public int Lowlink { get; set; }
		public T Value { get; protected set; }
		public List<T> Edges { get; protected set; }

		public void Add(T value)
		{
			if (!Edges.Contains(value))
				Edges.Add(value);
		}

		public void Add(Node<T> other)
		{
			if (!Edges.Contains(other.Value))
				Edges.Add(other.Value);
		}

		public bool Remove(T value)
		{
			return Edges.Remove(value);
		}

		public bool Remove(Node<T> node)
		{
			return Edges.Remove(node.Value);
		}

		public bool Contains(T value)
		{
			return Edges.Contains(value);
		}

		public bool Contains(Node<T> node)
		{
			return Edges.Contains(node.Value);
		}

		public int Count { get { return Edges.Count; } }

		public IEnumerator<T> GetEnumerator() { return Edges.GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return Edges.GetEnumerator(); }

		public Node() : this(default, null) { }
		public Node(T data) : this(data, null) { }
		public Node(T data, IEnumerable<T> list)
		{
			Index = null;
			Value = data;

			if (list == null)
				Edges = new List<T>();
			else
				Edges = list.ToList();
		}
	}
}
