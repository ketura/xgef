﻿
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Networking
{
	public class XEventArgs : EventArgs
	{
		public string EventName { get; set; }
		public bool Aborted { get; protected set; }
		public string AbortReason { get; protected set; }
		public object Aborter { get; protected set; }

		public Priority PriorityTier { get; set; }

		public void Abort(string reason, object aborter)
		{
			Aborted = true;
			AbortReason = reason;
			Aborter = aborter;
		}

		public void CancelAbort(string reason, object antiAborter)
		{
			Aborted = false;
			AbortReason = reason;
			Aborter = antiAborter;
		}
	}
}
