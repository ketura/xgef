﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class RequiresModTransformerTests
	{
		#region Extend Transform Test Cases

		public static string requireSupported = @"
[RequiresMod(""test"")]
namespace TestNamespace { }

[RequiresMod(""test"")]
public interface TestInterface { }

[RequiresMod(""test"")]
public struct TestStruct { }

[RequiresMod(""test"")]
public delegate TestDelegate();

[RequiresMod(""test"")]
public enum TestEnum { Value }

[RequiresMod(""test"")]
public class TestClass
{
	[RequiresMod(""test"")]
	public void TestMethod() { }

	[RequiresMod(""test"")]
	public TestClass() { }

	[RequiresMod(""test"")]
	public ~TestClass() { }

	[RequiresMod(""test"")]
	public int operator +(TestClass a, TestClass b) { return 0; }

	[RequiresMod(""test"")]
	public int TestField;

	[RequiresMod(""test"")]
	public event EventHandler TestEvent;

	[RequiresMod(""test"")]
	public int TestProperty { get; set; }

	[RequiresMod(""test"")]
	public int this[int i] { get { return i; } set { } }
}
";

		public static string requireExists = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"")]
public class TestClass { }
";

		public static string requireDontExist = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"", Exists: false)]
public class TestClass { }
";

		public static string requireMultipleExists = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"")]
[RequiresMod(""00000000-0000-0000-0000-000000000001"")]
public class TestClass { }
";

		public static string requireMultipleDontExist = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"", Exists: false)]
[RequiresMod(""00000000-0000-0000-0000-000000000001"", Exists: false)]
public class TestClass { }
";

		public static string requireMultipleMixed = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"", Exists: true)]
[RequiresMod(""00000000-0000-0000-0000-000000000001"", Exists: false)]
public class TestClass { }
";

		public static string requireNoArguments = @"
[RequiresMod]
public class TestClass { }
";

		public static string requireTooManyArguments = @"
[RequiresMod(""00000000-0000-0000-0000-000000000000"", Exists: true, Something: ""else"")]
public class TestClass { }
";

		#endregion

		[TestMethod()]
		public void RequiresMod_SupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			string result = analyzer.Analyze("test", requireSupported);
			Assert.IsTrue(new Regex("Overwrite").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void RequiresMod_RequiringModThatExistsLoadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000000" });
			string result = analyzer.Analyze("test", requireExists);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void RequiresMod_RequiringModThatExistsDeletesTag()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000000" });
			string result = analyzer.Analyze("test", requireExists);
			Assert.IsTrue(new Regex(@"RequiresMod").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void RequiresMod_RequiringModThatDoesNotExistUnloadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000001" });
			string result = analyzer.Analyze("test", requireExists);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void RequiresMod_ConflictingModThatDoesNotExistLoadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000001" });
			string result = analyzer.Analyze("test", requireDontExist);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void RequiresMod_ConflictingModThatDoesNotExistDeletesTag()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000001" });
			string result = analyzer.Analyze("test", requireDontExist);
			Assert.IsTrue(new Regex(@"RequiresMod").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void RequiresMod_ConflictingModThatExistsUnloadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000001" });
			string result = analyzer.Analyze("test", requireDontExist);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void RequiresMod_MultipleRequiredModsThatExistLoadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000001" });
			string result = analyzer.Analyze("test", requireMultipleExists);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void RequiresMod_MultipleConflictingModsThatDontExistLoadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000003" });
			string result = analyzer.Analyze("test", requireMultipleDontExist);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void RequiresMod_MultipleMixedModsLoadsMember()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			analyzer.AddMods(new string[] { "00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000003" });
			string result = analyzer.Analyze("test", requireMultipleMixed);
			Assert.IsTrue(new Regex(@"public class TestClass").Matches(result).Count == 1);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAttributeException))]
		public void RequiresMod_ThrowsExceptionIfNoArgumentsInAttributeTag()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			string result = analyzer.Analyze("test", requireNoArguments);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAttributeException))]
		public void RequiresMod_ThrowsExceptionIfMoreThanTwoArgumentsInAttributeTag()
		{
			Analyzer analyzer = new Analyzer(new RequiresModTransformer());
			string result = analyzer.Analyze("test", requireTooManyArguments);
		}	

	}
}
