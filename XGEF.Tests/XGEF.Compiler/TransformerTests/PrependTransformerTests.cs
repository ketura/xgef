﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class PrependTransformerTests
	{
		#region Extend Transform Test Cases
		public static string prependBase = @"
public class TestClass
{
	public void MethodPrependTest() 
  { 
    int OriginalMethodCode = 0;
  }
}
";

		public static string prependBaseComplex = @"
public class TestClass
{
	public void MethodPrependTest()
  { 
    int OriginalMethodCode = 0;
  }

  public TestClass()
  { 
    int OriginalConstructorCode = 0;
  }

  public ~TestClass()
  { 
    int OriginalFinalizerCode = 0;
  }
}
";

		public static string prependSupported = @"
public class TestClass
{
	[PrependFunction]
	public void MethodPrependTest()
  { 
    int NewMethodCode = 0;
  }

	[PrependFunction]
  public TestClass()
  { 
    int NewConstructorCode = 0;
  }

	[PrependFunction]
  public ~TestClass()
  { 
    int NewFinalizerCode = 0;
  }
}
";
		public static List<string> Unsupported = new List<string>()
		{
			@"
[PrependFunction]
public class TestClass { }
",
@"
[PrependFunction]
public interface TestInterface { }
",
@"
[PrependFunction]
public struct TestStruct { }
",
@"
[PrependFunction]
public delegate TestDelegate();
",
@"
[PrependFunction]
public enum TestEnum { Value }
",
@"
[PrependFunction]
public namespace TestNamespace { }
",
@"
public class TestClass
{
	[PrependFunction]
	public int TestField;
}
",
@"
public class TestClass
{
	[PrependFunction]
	public event EventHandler TestEvent;
}
",
@"
public class TestClass
{
	[PrependFunction]
	public int TestProperty { get; set; }
}
",
@"
public class TestClass
{
	[PrependFunction]
	public int this[int i] { get { return i; } set { } }
}
"
		};

		public static string prependSimple = @"
public class TestClass
{
	[PrependFunction]
	public void MethodPrependTest()
  { 
    int NewMethodCode = 0;
  }
}
";

		public static string prependConflict = @"
public class TestClass
{
  [Overwrite]
	[PrependFunction]
	public void MethodPrependTest()
  { 
    int NewMethodCode = 0;
  }
}
";

		public static string prependDuplicate = @"
public class TestClass
{
  [PrependFunction]
	[PrependFunction]
	public void MethodPrependTest()
  { 
    int NewMethodCode = 0;
  }
}
";
		#endregion


		[TestMethod()]
		public void PrependTransformer_MethodsArePrepended()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependBaseComplex, prependSupported);
			Assert.IsTrue(new Regex(@"int OriginalMethodCode = 0;").Matches(result).Count == 1, "Original method code is missing.");
			Assert.IsTrue(new Regex(@"int NewMethodCode = 0;").Matches(result).Count == 1, "New method code is missing.");
			Assert.IsTrue(new Regex(@"NewMethodCode.*OriginalMethodCode", RegexOptions.Singleline).Matches(result).Count == 1, "New method code does not come before old code.");
			Assert.IsTrue(new Regex(@"\[PrependFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void PrependTransformer_ConstructorsArePrepended()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependBaseComplex, prependSupported);
			Assert.IsTrue(new Regex(@"int OriginalConstructorCode = 0;").Matches(result).Count == 1, "Original constructor code is missing.");
			Assert.IsTrue(new Regex(@"int NewConstructorCode = 0;").Matches(result).Count == 1, "New constructor code is missing.");
			Assert.IsTrue(new Regex(@"NewConstructorCode.*OriginalConstructorCode", RegexOptions.Singleline).Matches(result).Count == 1, "New constructor code does not come before old code.");
			Assert.IsTrue(new Regex(@"\[PrependFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void PrependTransformer_FinalizerssArePrepended()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependBaseComplex, prependSupported);
			Assert.IsTrue(new Regex(@"int OriginalFinalizerCode = 0;").Matches(result).Count == 1, "Original finalizer code is missing.");
			Assert.IsTrue(new Regex(@"int NewFinalizerCode = 0;").Matches(result).Count == 1, "New finalizer code is missing.");
			Assert.IsTrue(new Regex(@"NewFinalizerCode.*OriginalFinalizerCode", RegexOptions.Singleline).Matches(result).Count == 1, "New finalizer code does not come before old code.");
			Assert.IsTrue(new Regex(@"\[PrependFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		//[ExpectedException(typeof(InvalidAttributeException))]
		public void PrependTransformer_ThrowsExceptionOnUnsupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			foreach (string test in Unsupported)
			{
				try
				{
					string result = analyzer.Analyze("test", test.Replace("[PrependFunction]", ""), test);
					Assert.Fail($"The below did not throw InvalidAttributeException:\n\n{test}");
				}
				catch (InvalidAttributeException)
				{
					continue;
				}
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(DuplicateAttributeException))]
		public void PrependTransformer_ThrowsExceptionIfDuplicateTags()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependDuplicate);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAncestorException))]
		public void PrependTransformer_ThrowsExceptionIfNothingToExtend()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependSimple);
		}

		[TestMethod()]
		[ExpectedException(typeof(ConflictingAttributeException))]
		public void PrependTransformer_ThrowsExceptionIfConflictingAttribute()
		{
			Analyzer analyzer = new Analyzer(new PrependFunctionTransformer());
			string result = analyzer.Analyze("test", prependBase, prependConflict);
		}

		[TestMethod]
		public void PrependTransformer_OnValidMatchesActualAttributeUsage()
		{
			var transformer = new PrependFunctionTransformer();
			MemberInfo info = typeof(PrependFunctionAttribute);
			var attribute = info.GetCustomAttributes<AttributeUsageAttribute>().First();
			foreach (AttributeTargets target in new AttributeTargets().GetValues())
			{
				if (attribute.ValidOn.HasFlag(target) && !transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was tagged with target {target.ToString()} but it is not contained within its ValidOn.");
				else if (!attribute.ValidOn.HasFlag(target) && transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was not tagged with target {target.ToString()} but it is somehow contained within its ValidOn.");
			}
		}

	}
}
