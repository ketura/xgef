﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class AppendTransformerTests
	{
		#region Extend Transform Test Cases
		public static string appendBase = @"
public class TestClass
{
	public void MethodAppendTest() 
  { 
    int OriginalMethodCode = 0;
  }
}
";

		public static string appendBaseComplex = @"
public class TestClass
{
	public void MethodAppendTest()
  { 
    int OriginalMethodCode = 0;
  }

  public TestClass()
  { 
    int OriginalConstructorCode = 0;
  }

  public ~TestClass()
  { 
    int OriginalFinalizerCode = 0;
  }
}
";

		public static string appendSupported = @"
public class TestClass
{
	[AppendFunction]
	public void MethodAppendTest()
  { 
    int NewMethodCode = 0;
  }

	[AppendFunction]
  public TestClass()
  { 
    int NewConstructorCode = 0;
  }

	[AppendFunction]
  public ~TestClass()
  { 
    int NewFinalizerCode = 0;
  }
}
";
		public static List<string> Unsupported = new List<string>()
		{
			@"
[AppendFunction]
public class TestClass { }
",
@"
[AppendFunction]
public interface TestInterface { }
",
@"
[AppendFunction]
public struct TestStruct { }
",
@"
[AppendFunction]
public delegate TestDelegate();
",
@"
[AppendFunction]
public enum TestEnum { Value }
",
@"
[AppendFunction]
public namespace TestNamespace { }
",
@"
public class TestClass
{
	[AppendFunction]
	public int TestField;
}
",
@"
public class TestClass
{
	[AppendFunction]
	public event EventHandler TestEvent;
}
",
@"
public class TestClass
{
	[AppendFunction]
	public int TestProperty { get; set; }
}
",
@"
public class TestClass
{
	[AppendFunction]
	public int this[int i] { get { return i; } set { } }
}
"
		};

		public static string appendSimple = @"
public class TestClass
{
	[AppendFunction]
	public void MethodAppendTest()
  { 
    int NewMethodCode = 0;
  }
}
";

		public static string appendConflict = @"
public class TestClass
{
  [Overwrite]
	[AppendFunction]
	public void MethodAppendTest()
  { 
    int NewMethodCode = 0;
  }
}
";

		public static string appendDuplicate = @"
public class TestClass
{
  [AppendFunction]
	[AppendFunction]
	public void MethodAppendTest()
  { 
    int NewMethodCode = 0;
  }
}
";
		#endregion


		[TestMethod()]
		public void AppendTransformer_MethodsAreAppended()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendBaseComplex, appendSupported);
			Assert.IsTrue(new Regex(@"int OriginalMethodCode = 0;").Matches(result).Count == 1, "Original method code is missing.");
			Assert.IsTrue(new Regex(@"int NewMethodCode = 0;").Matches(result).Count == 1, "New method code is missing.");
			Assert.IsTrue(new Regex(@"OriginalMethodCode.*NewMethodCode", RegexOptions.Singleline).Matches(result).Count == 1, "New method code does not come after old code.");
			Assert.IsTrue(new Regex(@"\[AppendFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void AppendTransformer_ConstructorsAreAppended()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendBaseComplex, appendSupported);
			Assert.IsTrue(new Regex(@"int OriginalConstructorCode = 0;").Matches(result).Count == 1, "Original constructor code is missing.");
			Assert.IsTrue(new Regex(@"int NewConstructorCode = 0;").Matches(result).Count == 1, "New constructor code is missing.");
			Assert.IsTrue(new Regex(@"OriginalConstructorCode.*NewConstructorCode", RegexOptions.Singleline).Matches(result).Count == 1, "New constructor code does not come after old code.");
			Assert.IsTrue(new Regex(@"\[AppendFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void AppendTransformer_FinalizerssAreAppended()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendBaseComplex, appendSupported);
			Assert.IsTrue(new Regex(@"int OriginalFinalizerCode = 0;").Matches(result).Count == 1, "Original constructor code is missing.");
			Assert.IsTrue(new Regex(@"int NewFinalizerCode = 0;").Matches(result).Count == 1, "New finalizer code is missing.");
			Assert.IsTrue(new Regex(@"OriginalFinalizerCode.*NewFinalizerCode", RegexOptions.Singleline).Matches(result).Count == 1, "New finalizer code does not come after old code.");
			Assert.IsTrue(new Regex(@"\[AppendFunction]").Matches(result).Count == 0);
		}

		[TestMethod()]
		//[ExpectedException(typeof(InvalidAttributeException))]
		public void AppendTransformer_ThrowsExceptionOnUnsupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			foreach (string test in Unsupported)
			{
				try
				{
					string result = analyzer.Analyze("test", test.Replace("[AppendFunction]", ""), test);
					Assert.Fail($"The below did not throw InvalidAttributeException:\n\n{test}");
				}
				catch (InvalidAttributeException)
				{
					continue;
				}
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(DuplicateAttributeException))]
		public void AppendTransformer_ThrowsExceptionIfDuplicateTags()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendDuplicate);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAncestorException))]
		public void AppendTransformer_ThrowsExceptionIfNothingToExtend()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendSimple);
		}

		[TestMethod()]
		[ExpectedException(typeof(ConflictingAttributeException))]
		public void AppendTransformer_ThrowsExceptionIfConflictingAttribute()
		{
			Analyzer analyzer = new Analyzer(new AppendFunctionTransformer());
			string result = analyzer.Analyze("test", appendBase, appendConflict);
		}

		[TestMethod]
		public void AppendTransformer_OnValidMatchesActualAttributeUsage()
		{
			var transformer = new AppendFunctionTransformer();
			MemberInfo info = typeof(AppendFunctionAttribute);
			var attribute = info.GetCustomAttributes<AttributeUsageAttribute>().First();
			foreach (AttributeTargets target in new AttributeTargets().GetValues())
			{
				if (attribute.ValidOn.HasFlag(target) && !transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was tagged with target {target.ToString()} but it is not contained within its ValidOn.");
				else if (!attribute.ValidOn.HasFlag(target) && transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was not tagged with target {target.ToString()} but it is somehow contained within its ValidOn.");
			}
		}

	}
}
