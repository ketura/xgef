﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass]
	public class TransformerTests
	{
		public static readonly Type[] AttributeTypes = new Type[]
		{
			typeof(OverwriteAttribute),
			typeof(ExtendAttribute)
		};

		[TestMethod]
		public void Transformer_OnValidMatchesActualAttributeUsage()
		{

		}
	}
}


/*
public static string BaseComplex = @"
namespace TestNamespace { }
public interface TestInterface { }
public struct TestStruct { }
public delegate TestDelegate();
public enum TestEnum { Value }

public class TestClass
{
		public void TestMethod() { }
		public TestClass() { }
		public ~TestClass() { }
		public int operator +(TestClass a, TestClass b) { return 0; }
		public int TestField;
		public event EventHandler TestEvent;
		public int TestProperty { get; set; }
		public int this[int i] { get { return i; } set { } }
}
";

public static string Supported = @"
[Extend]
namespace TestNamespace { }

[Extend]
public interface TestInterface { }

[Extend]
public struct TestStruct { }

[Extend]
public delegate TestDelegate();

[Extend]
public enum TestEnum { Value }

[Extend]
public class TestClass
{
	[Extend]
	public void TestMethod() { }

	[Extend]
	public TestClass() { }

	[Extend]
	public ~TestClass() { }

	[Extend]
	public int operator +(TestClass a, TestClass b) { return 0; }

	[Extend]
	public int TestField;

	[Extend]
	public event EventHandler TestEvent;

	[Extend]
	public int TestProperty { get; set; }

	[Extend]
	public int this[int i] { get { return i; } set { } }
}
";


	public static List<string> Unsupported = new List<string>()
		{
			@"
[Extend]
public class TestClass { }
",
@"
[Extend]
public interface TestInterface { }
",
@"
[Extend]
public struct TestStruct { }
",
@"
[Extend]
public delegate TestDelegate();
",
@"
[Extend]
public enum TestEnum { Value }
",
@"
[Extend]
namespace TestNamespace { }
",
@"
public class TestClass
{
	[Extend]
	public void TestMethod() { }
}
",
@"
public class TestClass
{
	[Extend]
	public TestClass() { }
}
",
@"
public class TestClass
{
	[Extend]
	public ~TestClass() { }
}
",
@"
public class TestClass
{
	[Extend]
	public int operator +(TestClass a, TestClass b) { return 0; }
}
",
@"
public class TestClass
{
	[Extend]
	public int TestField;
}
",
@"
public class TestClass
{
	[Extend]
	public event EventHandler TestEvent;
}
",
@"
public class TestClass
{
	[Extend]
	public int TestProperty { get; set; }
}
",
@"
public class TestClass
{
	[Extend]
	public int this[int i] { get { return i; } set { } }
}
"
		};

*/
