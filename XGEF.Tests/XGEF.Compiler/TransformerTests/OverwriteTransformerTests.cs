﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class OverwriteTransformerTests
	{
		#region Overwrite Transform Test Cases
		public static string overwriteBase = @"
//OverwriteBase
public class TestClass
{
	public void ShouldBeOverridden() { }
}
";

		public static string overwriteBaseComplex = @"
public interface TestInterface { }

public struct TestStruct { }

public delegate TestDelegate();

public enum TestEnum { Value }

public class TestClass
{
	public void TestMethod() { }

	public TestClass() { }

	public ~TestClass() { }

	public int operator +(TestClass a, TestClass b) { return 0; }

	public int TestField;

	public event EventHandler TestEvent;

	public int TestProperty { get; set; }

	public int this[int i] { get { return i; } set { } }
}
";

		public static string overwriteSupported = @"
[Overwrite]
public interface TestInterface { }

[Overwrite]
public struct TestStruct { }

[Overwrite]
public delegate TestDelegate();

[Overwrite]
public enum TestEnum { Value }

[Overwrite]
public class TestClass
{
	[Overwrite]
	public void TestMethod() { }

	[Overwrite]
	public TestClass() { }

	[Overwrite]
	public ~TestClass() { }

	[Overwrite]
	public int operator +(TestClass a, TestClass b) { return 0; }

	[Overwrite]
	public int TestField;

	[Overwrite]
	public event EventHandler TestEvent;

	[Overwrite]
	public int TestProperty { get; set; }

	[Overwrite]
	public int this[int i] { get { return i; } set { } }
}
";

		public static List<string> overwriteUnsupported = new List<string>()
		{
@"
[Overwrite]
namespace TestNamespace { }
"
		};

		public static string overwriteTag = @"
//OverwriteTag
[type: Overwrite]
public class TestClass { }";

		

		public static string overwriteSimple = @"
//OverwriteSimple
[Overwrite]
public class TestClass 
{ 
	public void TestMethod() { }
}
";

		public static string overwriteConflict = @"
//OverwriteConflict
[Overwrite]
[Extend]
public class TestClass { }
";

		public static string overwriteDuplicate = @"
//OverwriteDuplicate
[Overwrite]
[Overwrite]
public class TestClass { }
";
		#endregion


		[TestMethod()]
		public void OverwriteTransformer_AttributeStrippedFromSupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBaseComplex, overwriteSupported);
			Assert.IsTrue(new Regex("Overwrite").Matches(result).Count == 0);
			result = analyzer.Analyze("test", overwriteBase, overwriteTag);
			Assert.IsTrue(new Regex("Overwrite").Matches(result).Count == 0);
		}

		[TestMethod()]
		//[ExpectedException(typeof(InvalidAttributeException))]
		public void OverwriteTransformer_ThrowsExceptionOnUnsupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			foreach(string test in overwriteUnsupported)
			{
				try
				{
					string result = analyzer.Analyze("test", test.Replace("[Overwrite]", ""), test);
					Assert.Fail("Did not throw InvalidAttributeException.");
				}
				catch(InvalidAttributeException)
				{
					continue;
				}
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(DuplicateAttributeException))]
		public void OverwriteTransformer_ThrowsExceptionIfDuplicateTags()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteDuplicate);
		}

		[TestMethod()]
		public void OverwriteTransformer_ClassIsOverwritten()
		{
			Analyzer analyzer = new Analyzer(new ExplicitOverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBase, overwriteSimple);
			Assert.IsTrue(new Regex(@"TestMethod").Matches(result).Count == 1, "New class did not take priority.");
			Assert.IsTrue(new Regex(@"ShouldBeOverridden").Matches(result).Count == 0, "Old class was not removed.");
		}

		[TestMethod()]
		public void OverwriteTransformer_OverriddenClassHasNoOriginalMethods()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBase, overwriteSimple);
			Assert.IsTrue(new Regex("ShouldBeOverridden").Matches(result).Count == 0);
		}

		[TestMethod()]
		public void OverwriteTransformer_OverriddenClassInsertsNewMethods()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBase, overwriteSimple);
			Assert.IsTrue(new Regex("TestMethod").Matches(result).Count == 1);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAncestorException))]
		public void OverwriteTransformer_ThrowsExceptionIfNothingToOverwrite()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteSimple);
		}

		[TestMethod()]
		[ExpectedException(typeof(ConflictingAttributeException))]
		public void OverwriteTransformer_ThrowsExceptionIfConflictingAttribute()
		{
			Analyzer analyzer = new Analyzer(new OverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBase, overwriteConflict);
		}


		[TestMethod]
		public void OverwriteTransformer_OnValidMatchesActualAttributeUsage()
		{
			var transformer = new OverwriteTransformer();
			MemberInfo info = typeof(OverwriteAttribute);
			var attribute = info.GetCustomAttributes<AttributeUsageAttribute>().First();
			foreach (AttributeTargets target in new AttributeTargets().GetValues())
			{
				if (attribute.ValidOn.HasFlag(target) && !transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was tagged with target {target.ToString()} but it is not contained within its ValidOn.");
				else if(!attribute.ValidOn.HasFlag(target) && transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was not tagged with target {target.ToString()} but it is somehow contained within its ValidOn.");
			}
		}
	}
}
