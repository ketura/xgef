﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class ExplicitOverrideTests
	{
		#region Overwrite Transform Test Cases
		public static string overwriteBase = @"
//OverwriteBase
public class TestClass
{
	public void ShouldBeOverridden() { }
}
";

		public static string overwriteConflict = @"
//OverwriteConflict
[Overwrite]
[Extension]
public class TestClass 
{ 
	public void NewMethod() { }
}
";

		#endregion

		[TestMethod()]
		[ExpectedException(typeof(UndeclaredOverwriteException))]
		public void OverwriteTransformer_ThrowsExceptionIfOverwriteNotDeclared()
		{
			Analyzer analyzer = new Analyzer(new ExplicitOverwriteTransformer());
			string result = analyzer.Analyze("test", overwriteBase, overwriteBase);
		}

		//[TestMethod()]
		//public void OverwriteTransformer_ClassIsOverwritten()
		//{
		//	Analyzer analyzer = new Analyzer(new ExplicitOverwriteTransformer());
		//	string result = analyzer.Analyze("test", overwriteBase, overwriteConflict);
		//	Assert.IsTrue(new Regex(@"NewMethod").Matches(result).Count == 1, "New class did not take priority.");
		//	Assert.IsTrue(new Regex(@"ShouldBeOverridden").Matches(result).Count == 0, "Old class was not removed.");
		//}



	}
}
