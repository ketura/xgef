﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using XGEF.Compiler;

namespace XGEF.Tests.Compiler
{
	[TestClass()]
	public class ExtendTransformerTests
	{
		#region Extend Transform Test Cases
		public static string extendBase = @"
public class TestClass
{
	public void ShouldBeExtended() { }
}
";

		public static string extendBaseComplex = @"
public class TestClass { public int ClassUnaffected; }

public interface TestInterface { int InterfaceUnaffected; }

public struct TestStruct { public int StructUnaffected; }

public enum TestEnum { EnumUnaffected }


";

		public static string extendSupported = @"
[Extend]
public interface TestInterface { int InterfaceTest; }

[Extend]
public struct TestStruct { public int StructTest; }

[Extend]
public enum TestEnum { EnumTest }

[Extend]
public class TestClass { public int ClassTest; }
";

		public static List<string> extendUnsupported = new List<string>()
		{
@"
[Extend]
public delegate TestDelegate();
",
@"
[Extend]
namespace TestNamespace { }
",
@"
public class TestClass
{
	[Extend]
	public void TestMethod() { }
}
",
@"
public class TestClass
{
	[Extend]
	public TestClass() { }
}
",
@"
public class TestClass
{
	[Extend]
	public ~TestClass() { }
}
",
@"
public class TestClass
{
	[Extend]
	public int operator +(TestClass a, TestClass b) { return 0; }
}
",
@"
public class TestClass
{
	[Extend]
	public int TestField;
}
",
@"
public class TestClass
{
	[Extend]
	public event EventHandler TestEvent;
}
",
@"
public class TestClass
{
	[Extend]
	public int TestProperty { get; set; }
}
",
@"
public class TestClass
{
	[Extend]
	public int this[int i] { get { return i; } set { } }
}
"
		};

		public static string extendTag = @"
[type: Extend]
public class TestClass { public int ClassTest; }";

		public static string extendSimple = @"
[Extend]
public class TestClass 
{ 
	public void TestMethod() { }
}
";

		public static string extendConflict = @"
[Overwrite]
[Extend]
public class TestClass { }
";

		public static string extendDuplicate = @"
[Extend]
[Extend]
public class TestClass { }
";
		#endregion


		[TestMethod()]
		public void ExtendTransformer_ClassesAreExtended()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendBaseComplex, extendSupported);
			Assert.IsTrue(new Regex(@"public int ClassTest;").Matches(result).Count == 1);
			Assert.IsTrue(new Regex(@"public int ClassUnaffected;").Matches(result).Count == 1);

			result = analyzer.Analyze("test", extendBase, extendTag);
			Assert.IsTrue(new Regex(@"public int ClassTest;").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void ExtendTransformer_StructsAreExtended()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendBaseComplex, extendSupported);
			Assert.IsTrue(new Regex(@"public int StructTest;").Matches(result).Count == 1);
			Assert.IsTrue(new Regex(@"public int StructUnaffected;").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void ExtendTransformer_InterfacesAreExtended()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendBaseComplex, extendSupported);
			Assert.IsTrue(new Regex(@"int InterfaceTest;").Matches(result).Count == 1);
			Assert.IsTrue(new Regex(@"int InterfaceUnaffected;").Matches(result).Count == 1);
		}

		[TestMethod()]
		public void ExtendTransformer_EnumsAreExtended()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendBaseComplex, extendSupported);
			Assert.IsTrue(new Regex(@"EnumTest").Matches(result).Count == 1);
			Assert.IsTrue(new Regex(@"EnumUnaffected").Matches(result).Count == 1);
		}

		[TestMethod()]
		//[ExpectedException(typeof(InvalidAttributeException))]
		public void ExtendTransformer_ThrowsExceptionOnUnsupportedTypes()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			foreach (string test in extendUnsupported)
			{
				try
				{
					string result = analyzer.Analyze("test", test.Replace("[Extend]", ""), test);
					Assert.Fail($"The below did not throw InvalidAttributeException:\n\n{test}");
				}
				catch (InvalidAttributeException)
				{
					continue;
				}
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(DuplicateAttributeException))]
		public void ExtendTransformer_ThrowsExceptionIfDuplicateTags()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendDuplicate);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidAncestorException))]
		public void ExtendTransformer_ThrowsExceptionIfNothingToExtend()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendSimple);
		}

		[TestMethod()]
		[ExpectedException(typeof(ConflictingAttributeException))]
		public void ExtendTransformer_ThrowsExceptionIfConflictingAttribute()
		{
			Analyzer analyzer = new Analyzer(new ExtendTransformer());
			string result = analyzer.Analyze("test", extendBase, extendConflict);
		}

		[TestMethod]
		public void ExtendTransformer_OnValidMatchesActualAttributeUsage()
		{
			var transformer = new ExtendTransformer();
			MemberInfo info = typeof(ExtendAttribute);
			var attribute = info.GetCustomAttributes<AttributeUsageAttribute>().First();
			foreach (AttributeTargets target in new AttributeTargets().GetValues())
			{
				if (attribute.ValidOn.HasFlag(target) && !transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was tagged with target {target.ToString()} but it is not contained within its ValidOn.");
				else if (!attribute.ValidOn.HasFlag(target) && transformer.ValidOn.Contains(target))
					Assert.Fail($"{transformer.GetType().Name} was not tagged with target {target.ToString()} but it is somehow contained within its ValidOn.");
			}
		}

	}
}
