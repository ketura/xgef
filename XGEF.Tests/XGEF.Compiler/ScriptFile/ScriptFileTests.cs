﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF.Compiler;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace XGEF.Tests.Compiler
{
	[TestClass]
	public class ScriptFileTests
	{
		[TestMethod]
		public void ScriptFile_ReconstructedCodeSameAsOriginal()
		{
			string original = File.ReadAllText("../../XGEF.Compiler/ScriptFile/TestScript.cs");
			original = ScriptFile.FilterTextBasic(original);

			ScriptFile result = ScriptFile.AnalyzeFile("test", original);
			int memberCount = result.Count;
			string reconstructed = result.ReconstructCode();

			ScriptFile result2 = ScriptFile.AnalyzeFile("test", reconstructed);
			int memberCount2 = result2.Count;
			string reconstructed2 = result.ReconstructCode();

			Assert.AreEqual(memberCount, memberCount2, "Member count of first and second parse do not match.");
			Assert.AreEqual(reconstructed, reconstructed2, "Output string of first parse and second parse do not match.");

			foreach(var root in result.TopLevel.Values)
			{
				Assert.IsTrue(root.CompareTo(result2.MemberWithFullName(root.FullName)), $"Original's {root.FullName} fails to compare with reconstructed {root.FullName}");
			}
		
		}
	}
}
