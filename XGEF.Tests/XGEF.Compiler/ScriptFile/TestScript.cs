﻿using System;
using System.Text;

namespace Namespace
{
	[Attribute]
	public enum OuterEnum { EnumVal1, EnumVal2 }
	[Attribute]
	public enum OuterSetEnum { EnumVal1 = 1, EnumVal2 = 2, EnumValII = 2 }

	public class Class<T> //comment
		where T : object
	{
		[Attribute]
		public Class() : this(1) { }
		[Attribute("some value")]
		public Class(int i) : base() { }
		[Attribute]
		public const int ConstantInt = 0;
		[Attribute]
		public int IntField;
		[Attribute]
		~Class() { }
		[Attribute]
		public int Method1() { }
		public int Method1(int Override) { }
		[Attribute("some value")]
		public void Method2() => throw new NotImplementedException();


		public (int, string, int) Method3() { }
		[Attribute("some value")]
		public partial void PartialMethod(int i);
		[Attribute("some value")]
		public int Property { get; set; }
		[Attribute("some value")]
		public int Property2 => throw new NotImplementedException();
		[Attribute("some value")]
		public int this[int index]
		{
			get { return 0; }
			set { }
		}
		[Attribute("some value")]
		public static Class operator +(Class a, Class b)
		{
			return a + b;
		}
		public delegate void Delegate();
		[Attribute]
		public event Delegate Event;
		[Attribute]
		public class InnerClass { }
		[Attribute]
		public interface InnerInterface { }
		[Attribute]
		public struct InnerStruct { }
		[Attribute]
		public enum Enum { EnumVal1, EnumVal2 }
		[Attribute]
		public enum SetEnum { EnumVal1 = 1, EnumVal2 = 2 }

	}

	public delegate void OuterDelegate();

	public interface OuterInterface
	{
		int Function();
		int Property { get; }
	}

	public struct OuterStruct
	{
		public int OuterStructIntField;
		public void OuterStructMethod()
		{
			Console.WriteLine("Hello from inside the OuterStruct!");
		}
		public int Property { get; set; }
		public class InnerClass : Class<int> { }
		public interface InnerInterface { }
		public struct InnerStruct { }
	}

	namespace NestedNamespace
	{
		public class NNClass
		{
			public NNClass() { }
			~NNClass() { }
		}

		public delegate void NNDelegate();

		public interface NNInterface
		{
			int Function();
			int Property { get; }
		}
		public struct NNStruct
		{
			public const int ConstantInt = 0;
		}
	}
	
}
