﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using NetcodeIO.NET;

using XGEF.Core;
using XGEF.Core.Events;
using XGEF.Core.Networking;
using XGEF.Game.Networking;

using XGEF.Game.Events;

using XGEF.Tests;


namespace XGEF.Tests.Core.Networking
{
	[TestClass]
	public class ReliableIOAdapterTests
	{
		private readonly int SleepTime = 2000;
		private static ushort SharedPort { get; set; } = 1300;
		private static ushort ServerPort { get; set; } = 2300;


		[TestMethod]
		public void ReliableIOAdapter_ServerAdapterConnectsToClient()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var client = new Client();
			TokenFactory factory = new TokenFactory(1, CoreNetworkSystem.PrivateKey);
			var serverList = new IPEndPoint[] { new IPEndPoint(IPAddress.Loopback, serverPort) };
			var token = factory.GenerateConnectToken(serverList, 500, 500, 1, 1, new byte[0]);
			

			bool connected = false;
			client.OnStateChanged +=
				(ClientState state) =>
				{
					if(state == ClientState.Connected)
					{
						connected = true;
					}
				};

			NetcodeIOServerAdapter serverAdapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);
			client.Connect(token);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			for (int i = 0; i < SleepTime; ++i)
			{
				serverAdapter.Update();
				Thread.Sleep(1);
			}

			Assert.IsTrue(connected, "Server adapter timed out or otherwise did not connect to the Netcode.IO client.");
		}

		[TestMethod]
		public void ReliableIOAdapter_ServerAdapterMessageToClientReceived()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var client = new Client();
			TokenFactory factory = new TokenFactory(1, CoreNetworkSystem.PrivateKey);
			var serverList = new IPEndPoint[] { new IPEndPoint(IPAddress.Loopback, serverPort) };
			var token = factory.GenerateConnectToken(serverList, 500, 500, 1, 1, new byte[0]);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;
			int messages = 0;

			NetcodeIOServerAdapter serverAdapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);

			serverAdapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					serverAdapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
				};

			client.OnMessageReceived +=
				(byte[] payload, int payloadSize) =>
				{
					messageRecieved = true;
					NetcodeMetadataPacket packet = new NetcodeMetadataPacket(payload.Take(payloadSize).ToArray());
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
					messages++;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			client.Connect(token);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			for (int i = 0; i < SleepTime; ++i)
			{
				serverAdapter.Update();
				Thread.Sleep(1);
			}

			Assert.IsTrue(messageRecieved, "Netcode.IO client never received a message from the server adapter.");
			Assert.AreEqual(sentMessage, message, $"Netcode.IO client did not recieve the expected '{sentMessage}' message from the server adapter; received {messages}.");
		}

		[TestMethod]
		public void ReliableIOAdapter_ServerAdapterConnectsToClientAdapter()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new ReliableIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new ReliableIOServerAdapter("127.0.0.1", serverPort);

			bool connected = false;
			serverAdapter.OnClientConnected +=
				(IClientAdapter client) =>
				{
					connected = true;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			clientAdapter.Connect(serverAdapter.FullIPAddress);

			for (int i = 0; i < SleepTime; ++i)
			{
				clientAdapter.Update();
				serverAdapter.Update();
				Thread.Sleep(1);
			}

			//Note that this means manually debugging is not going to work so well.  Sorry!
			

			Assert.IsTrue(connected, "Client adapter timed out or otherwise did not connect to the server adapter.");
		}

		[TestMethod]
		public void ReliableIOAdapter_ServerAdapterMessageToClientAdapterReceived()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new ReliableIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new ReliableIOServerAdapter("127.0.0.1", serverPort);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;
			bool sent = false;

			serverAdapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					if (!sent)
					{
						serverAdapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
						sent = true;
					}
				};

			clientAdapter.OnMessageReceived +=
				(INetworkAdapter sender, NetcodeMetadataPacket packet) =>
				{
					messageRecieved = true;
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();

			clientAdapter.Connect(serverAdapter.FullIPAddress);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			for (int i = 0; i < SleepTime; ++i)
			{
				clientAdapter.Update();
				serverAdapter.Update();
				Thread.Sleep(1);
			}

			Assert.IsTrue(messageRecieved, "Adapter client never received a message from the server adapter.");
			Assert.AreEqual(sentMessage, message, $"Adapter client did not recieve the expected '{sentMessage}' message from the server adapter.");
		}

		[TestMethod]
		public void ReliableIOAdapter_ClientAdapterMessageToServerAdapterReceived()
		{

			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new ReliableIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new ReliableIOServerAdapter("127.0.0.1", serverPort);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;

			bool sent = false;

			serverAdapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					if (!sent)
					{
						clientAdapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
						sent = true;
					}
				};

			serverAdapter.OnMessageReceived +=
				(INetworkAdapter sender, NetcodeMetadataPacket packet) =>
				{
					messageRecieved = true;
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();

			clientAdapter.Connect(serverAdapter.FullIPAddress);

			for (int i = 0; i < SleepTime; ++i)
			{
				clientAdapter.Update();
				serverAdapter.Update();
				Thread.Sleep(1);
			}

			Assert.IsTrue(messageRecieved, "Adapter server never received a message from the client adapter.");
			Assert.AreEqual(message, sentMessage, $"Adapter server did not recieve the expected '{sentMessage}' message from the client adapter.");
		}
	}
}
