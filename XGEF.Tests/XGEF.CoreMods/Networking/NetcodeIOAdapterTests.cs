﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using NetcodeIO.NET;
using XGEF.Core.Events;
using XGEF.Core.Networking;
using XGEF.Game.Networking;

using XGEF.Game.Events;
using XGEF.Tests;


namespace XGEF.Tests.Core.Networking
{
	[TestClass]
	public class NetcodeIOAdapterTests
	{
		private readonly int SleepTime = 2000;
		private static ushort SharedPort { get; set; } = 1337;
		private static ushort ServerPort { get; set; } = 2401;


		[TestMethod]
		public void NetcodeIOAdapter_ServerAdapterConnectsToClient()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var client = new Client();
			TokenFactory factory = new TokenFactory(1, CoreNetworkSystem.PrivateKey);
			var serverList = new IPEndPoint[] { new IPEndPoint(IPAddress.Loopback, serverPort) };
			var token = factory.GenerateConnectToken(serverList, 500, 500, 1, 1, new byte[0]);
			

			bool connected = false;
			client.OnStateChanged +=
				(ClientState state) =>
				{
					if(state == ClientState.Connected)
					{
						connected = true;
					}
				};

			NetcodeIOServerAdapter adapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);
			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			client.Connect(token);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			Thread.Sleep(SleepTime);

			Assert.IsTrue(connected, "Server adapter timed out or otherwise did not connect to the Netcode.IO client.");
		}

		[TestMethod]
		public void NetcodeIOAdapter_ServerAdapterMessageToClientReceived()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var client = new Client();
			TokenFactory factory = new TokenFactory(1, CoreNetworkSystem.PrivateKey);
			var serverList = new IPEndPoint[] { new IPEndPoint(IPAddress.Loopback, serverPort) };
			var token = factory.GenerateConnectToken(serverList, 500, 500, 1, 1, new byte[0]);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;

			NetcodeIOServerAdapter adapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);

			adapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					adapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
				};

			client.OnMessageReceived +=
				(byte[] payload, int payloadSize) =>
				{
					messageRecieved = true;
					NetcodeMetadataPacket packet = new NetcodeMetadataPacket(payload.Take(payloadSize).ToArray());
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			client.Connect(token);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			Thread.Sleep(SleepTime);

			Assert.IsTrue(messageRecieved, "Netcode.IO client never received a message from the server adapter.");
			Assert.AreEqual(sentMessage, message, $"Netcode.IO client did not recieve the expected {sentMessage} message from the server adapter.");
		}

		[TestMethod]
		public void NetcodeIOAdapter_ServerAdapterConnectsToClientAdapter()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new NetcodeIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);

			bool connected = false;
			serverAdapter.OnClientConnected +=
				(IClientAdapter client) =>
				{
					connected = true;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			clientAdapter.Connect(serverAdapter.FullIPAddress);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			Thread.Sleep(SleepTime);

			Assert.IsTrue(connected, "Client adapter timed out or otherwise did not connect to the server adapter.");
		}

		[TestMethod]
		public void NetcodeIOAdapter_ServerAdapterMessageToClientAdapterReceived()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new NetcodeIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;

			serverAdapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					serverAdapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
				};

			clientAdapter.OnMessageReceived +=
				(INetworkAdapter sender, NetcodeMetadataPacket packet) =>
				{
					messageRecieved = true;
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			clientAdapter.Connect(serverAdapter.FullIPAddress);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			Thread.Sleep(SleepTime);

			Assert.IsTrue(messageRecieved, "Adapter client never received a message from the server adapter.");
			Assert.AreEqual(sentMessage, message, $"Adapter client did not recieve the expected {sentMessage} message from the server adapter.");
		}

		[TestMethod]
		public void NetcodeIOAdapter_ClientAdapterMessageToServerAdapterReceived()
		{
			ushort port = SharedPort++;
			ushort serverPort = ServerPort++;
			var clientAdapter = new NetcodeIOClientAdapter("127.0.0.1", port);
			var serverAdapter = new NetcodeIOServerAdapter("127.0.0.1", serverPort);

			string sentMessage = "test";
			bool messageRecieved = false;
			string message = null;

			serverAdapter.OnClientConnected +=
				(IClientAdapter c) =>
				{
					clientAdapter.SendMessage(new MessageEventArgs() { MessageID = 1, Message = sentMessage });
				};

			serverAdapter.OnMessageReceived +=
				(INetworkAdapter sender, NetcodeMetadataPacket packet) =>
				{
					messageRecieved = true;
					message = DefaultBitstreamConverter.DeserializeJsonBitstream<MessageEventArgs>(packet.Payload).Message;
				};

			TestSystemLoader.PopulateCoreSystem<CoreEventSystem, EventSystem>().Init();
			clientAdapter.Connect(serverAdapter.FullIPAddress);

			//Note that this means manually debugging is not going to work so well.  Sorry!
			Thread.Sleep(SleepTime);

			Assert.IsTrue(messageRecieved, "Adapter server never received a message from the client adapter.");
			Assert.AreEqual(sentMessage, message, $"Adapter server did not recieve the expected {sentMessage} message from the client adapter.");
		}
	}
}
