﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Tests
{
	[TestClass()]
	public class ModLoaderTests
	{
		protected class ModDirectorySandbox : IDisposable
		{
			public ModDirectorySandbox()
			{
				MakeTempDirLayout();
			}

			#region IDisposable Support
			private bool disposedValue = false; // To detect redundant calls

			protected virtual void Dispose(bool disposing)
			{
				if (!disposedValue)
				{
					if (disposing)
					{
						ClearTempDirLayout();
					}

					// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
					// TODO: set large fields to null.

					disposedValue = true;
				}
			}

			// This code added to correctly implement the disposable pattern.
			public void Dispose()
			{
				// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
				Dispose(true);
				// TODO: uncomment the following line if the finalizer is overridden above.
				// GC.SuppressFinalize(this);
			}
			#endregion

			public string TempPath { get; protected set; }
			public string ModAPath { get; protected set; }
			public string ModBPath { get; protected set; }
			public string ModCPath { get; protected set; }
			public ModInfo ModA { get; protected set; }
			public ModInfo ModB { get; protected set; }
			public ModInfo ModC { get; protected set; }


			protected void MakeRandomTempDir()
			{
				if (TempPath != null)
					ClearTempDirLayout();

				do
				{
					TempPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
				} while (Directory.Exists(TempPath));

				Directory.CreateDirectory(TempPath);
			}

			protected string MakeTempDirLayout()
			{
				MakeRandomTempDir();

				ModA = AddMod("A");
				ModB = AddMod("B");
				ModC = AddMod("C");

				ModAPath = ModA.RootLocation;
				ModBPath = ModB.RootLocation;
				ModCPath = ModC.RootLocation;

				return TempPath;
			}

			public ModInfo AddMod(string name, bool includeInfo = true, bool createSubDirs = true)
			{
				string path = Directory.CreateDirectory(TempPath).FullName;
				ModInfo info = GenerateModInfo(name, path);
				path = info.RootLocation;
				if (createSubDirs)
				{
					Directory.CreateDirectory(Path.Combine(path, "Engine"));
					Directory.CreateDirectory(Path.Combine(path, "Content"));
				}

				if (includeInfo)
				{
					WriteInfoToPath(info, path);
				}

				return info;
			}

			public ModInfo GenerateModInfo(string name, string path)
			{
				path = Directory.CreateDirectory(Path.Combine(path, name)).FullName;
				var info = ModInfo.GetNewModInfo();
				info.ModName = name;
				info.SetLocation(path);
				return info;
			}

			public void WriteInfoToPath(ModInfo info, string path)
			{
				string json = JsonSerializer.SerializeJson(info, JsonSerializer.TemplateSettings);
				File.WriteAllText(Path.Combine(path, ModInfo.DefaultFilename), json);
			}

			protected void ClearTempDirLayout()
			{
				Directory.Delete(TempPath, true);
				TempPath = null;
			}

		}



		[TestMethod()]
		[ExpectedException(typeof(InvalidOperationException))]
		public void ModLoader_SecondInitializationFails()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			XGEFSettings settings = manager.Settings;
			settings.CoreModAssemblyName = "CoreMods_SecondInitializationFails";
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(settings);
			loader.PreInit();
			loader.PreInit();
		}

		[TestMethod()]
		public void ModLoader_CoreModsIsLoadedDuringInitialization()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			XGEFSettings settings = manager.Settings;
			settings.CoreModAssemblyName = "CoreMods_CoreModsIsLoadedDuringInitialization";

			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(settings);
			loader.PreInit();
			Assert.IsTrue(loader.LoadedMods.Count > 0);
			Assert.IsTrue(loader.LoadedMods.ContainsKey(ModInfo.CoreModID));
			Assert.IsTrue(loader.CoreMod != null);
			Assert.IsTrue(loader.CoreMod.IsCoreMod);
		}

		[TestMethod()]
		public void ModLoader_ModInfoFileIsLoaded()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.AllMods.Count == 4);
				Assert.IsTrue(loader.AllMods.Any(x => x.ModName == "A"));
				Assert.IsTrue(loader.AllMods.Any(x => x.ModName == "B"));
				Assert.IsTrue(loader.AllMods.Any(x => x.ModName == "B"));
			}
		}

		[TestMethod()]
		public void ModLoader_ModPathWithNoInfoFileIsNotLoaded()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.AddMod("D", false);
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.AllMods.Count == 4);
				Assert.IsTrue(!loader.AllMods.Any(x => x.ModName == "D"));
			}
		}

		[TestMethod()]
		public void ModLoader_ModInfoFileNotInRootIsNotLoaded()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.AddMod("D", false);
				ModInfo d = sandbox.GenerateModInfo("D", sandbox.TempPath);
				sandbox.WriteInfoToPath(d, Path.Combine(d.RootLocation, "Engine"));
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.AllMods.Count == 4);
				Assert.IsTrue(!loader.AllMods.Any(x => x.ModName == "D"));
			}
		}

		[TestMethod()]
		public void ModLoader_MissingDependencyUnloadsMod()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.RequiredMods.Add(Guid.NewGuid());
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 3);
				Assert.IsTrue(loader.UnloadedMods.Count == 1);
				Assert.IsTrue(!loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_ExistingDependencyLoadsMod()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.RequiredMods.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_ExistingConflictUnloadsMod()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.ConflictingMods.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 3);
				Assert.IsTrue(loader.UnloadedMods.Count == 1);
				Assert.IsTrue(!loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_MissingConflictLoadsMod()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.ConflictingMods.Add(Guid.NewGuid());
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_LoadBeforeModLoadedWithLowerPriority()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseBefore.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods[sandbox.ModA.ModID].LoadPriority > loader.LoadedMods[sandbox.ModB.ModID].LoadPriority);
			}
		}

		[TestMethod()]
		public void ModLoader_MissingLoadBeforeStillLoads()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseBefore.Add(Guid.NewGuid());
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_LoadAfterModLoadedWithHigherPriority()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseAfter.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods[sandbox.ModA.ModID].LoadPriority < loader.LoadedMods[sandbox.ModB.ModID].LoadPriority);
			}
		}

		[TestMethod()]
		public void ModLoader_MissingLoadAfterStillLoads()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseAfter.Add(Guid.NewGuid());
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[ExpectedException(typeof(CircularReferenceException))]
		[TestMethod()]
		public void ModLoader_SimpleCycleThrowsException()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			manager.Settings.IgnoreCircularDependencies = RequirementHandling.None;
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseBefore.Add(sandbox.ModB.ModID);
				sandbox.ModB.LoadTheseBefore.Add(sandbox.ModA.ModID);
				sandbox.ModA.SaveToFile();
				sandbox.ModB.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });
			}
		}

		[ExpectedException(typeof(CircularReferenceException))]
		[TestMethod()]
		public void ModLoader_ComplexCycleThrowsException()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			manager.Settings.IgnoreCircularDependencies = RequirementHandling.None;
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseBefore.Add(sandbox.ModB.ModID);
				sandbox.ModB.LoadTheseBefore.Add(sandbox.ModC.ModID);
				sandbox.ModC.LoadTheseBefore.Add(sandbox.ModA.ModID);
				sandbox.ModA.SaveToFile();
				sandbox.ModB.SaveToFile();
				sandbox.ModC.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });
			}
		}

		[TestMethod()]
		public void ModLoader_IgnoreCircularDependencyLoadsMods()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			manager.Settings.IgnoreCircularDependencies = RequirementHandling.All;
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.LoadTheseBefore.Add(sandbox.ModB.ModID);
				sandbox.ModB.LoadTheseBefore.Add(sandbox.ModC.ModID);
				sandbox.ModC.LoadTheseBefore.Add(sandbox.ModA.ModID);
				sandbox.ModA.SaveToFile();
				sandbox.ModB.SaveToFile();
				sandbox.ModC.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "B"));
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "C"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "B"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "C"));
			}
		}

		[TestMethod()]
		public void ModLoader_DeloadedModInvalidatesDependedingMods()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.RequiredMods.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				sandbox.ModB.RequiredMods.Add(Guid.NewGuid());
				sandbox.ModB.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 2);
				Assert.IsTrue(loader.UnloadedMods.Count == 2);
				Assert.IsTrue(!loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.LoadedMods.Values.Any(x => x.ModName == "B"));
				Assert.IsTrue(loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(loader.UnloadedMods.Values.Any(x => x.ModName == "B"));
			}
		}

		[TestMethod()]
		public void ModLoader_IgnoreModRequirementsLoadsMods()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			manager.Settings.IgnoreModRequirements = RequirementHandling.All;
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.RequiredMods.Add(Guid.NewGuid());
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}

		[TestMethod()]
		public void ModLoader_IgnoreModConflictsLoadsMods()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(ModLoader), typeof(AssetIndex));
			ModLoader loader = manager.GetSystem<ModLoader>();
			manager.Settings.IgnoreModConflicts = RequirementHandling.All;
			loader.LoadSettings(manager.Settings);

			using (var sandbox = new ModDirectorySandbox())
			{
				sandbox.ModA.ConflictingMods.Add(sandbox.ModB.ModID);
				sandbox.ModA.SaveToFile();
				loader.LoadMods(new List<string>() { sandbox.TempPath });

				Assert.IsTrue(loader.LoadedMods.Count == 4);
				Assert.IsTrue(loader.UnloadedMods.Count == 0);
				Assert.IsTrue(loader.LoadedMods.Values.Any(x => x.ModName == "A"));
				Assert.IsTrue(!loader.UnloadedMods.Values.Any(x => x.ModName == "A"));
			}
		}
	}
}