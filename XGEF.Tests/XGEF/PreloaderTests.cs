﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Game.Stats;

namespace XGEF.Tests
{
	[TestClass()]
	public class PreloaderTests
	{
		[TestMethod()]
		public void Preloader_LoadByNamesParamsTest()
		{
			SystemManager manager = Preloader.LoadByNames("LogSystem");

			Assert.IsTrue(manager.Utilities.Count == 1);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
		}

		[TestMethod()]
		public void Preloader_LoadByNamesListTest()
		{
			SystemManager manager = Preloader.LoadByNames(new List<string>() { "LogSystem" });

			Assert.IsTrue(manager.Utilities.Count == 1);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
		}

		[TestMethod()]
		public void Preloader_DefaultManagerContainsDefaultSystems()
		{
			SystemManager manager = Preloader.LoadDefault();

			Assert.IsTrue(manager.Systems.Count + manager.Utilities.Count == 3);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("ModLoader") && manager.Utilities["ModLoader"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("AssetIndex") && manager.Utilities["AssetIndex"] != null);
		}

		[TestMethod()]
		public void Preloader_LoadDefaultByNamesTest()
		{

			SystemManager manager = Preloader.LoadDefaultByNames("XGEF.Game.Stats.CoreStatSystem");

			Assert.IsTrue(manager.Systems.Count + manager.Utilities.Count == 3);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("ModLoader") && manager.Utilities["ModLoader"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("AssetIndex") && manager.Utilities["AssetIndex"] != null);
		}

		[TestMethod()]
		public void Preloader_LoadByTypesParamsTest()
		{
			SystemManager manager = Preloader.LoadByTypes(typeof(LogSystem));

			Assert.IsTrue(manager.Utilities.Count == 1);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
		}

		[TestMethod()]
		public void Preloader_LoadByTypesTest()
		{
			SystemManager manager = Preloader.LoadByTypes(new List<Type>() { typeof(LogSystem) });

			Assert.IsTrue(manager.Utilities.Count == 1);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
		}

		[TestMethod()]
		public void Preloader_LoadDefaultByTypesTest()
		{
			SystemManager manager = Preloader.LoadDefaultWithTypes(typeof(CoreStatSystem));

			Assert.IsTrue(manager.Systems.Count + manager.Utilities.Count == 4);
			Assert.IsTrue(manager.Utilities.ContainsKey("LogSystem") && manager.Utilities["LogSystem"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("ModLoader") && manager.Utilities["ModLoader"] != null);
			Assert.IsTrue(manager.Utilities.ContainsKey("AssetIndex") && manager.Utilities["AssetIndex"] != null);
			Assert.IsTrue(manager.Systems.ContainsKey("CoreStatSystem") && manager.Systems["CoreStatSystem"] != null);
		}

		
	}
}