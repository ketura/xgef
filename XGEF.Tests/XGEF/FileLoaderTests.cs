﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Tests
{
	[TestClass()]
	public class FileLoaderTests
	{

		protected class TempDirectorySandbox : IDisposable
		{

			public TempDirectorySandbox()
			{
				MakeTempDirLayout();
			}

			#region IDisposable Support
			private bool disposedValue = false; // To detect redundant calls

			protected virtual void Dispose(bool disposing)
			{
				if (!disposedValue)
				{
					if (disposing)
					{
						ClearTempDirLayout();
					}

					// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
					// TODO: set large fields to null.

					disposedValue = true;
				}
			}

			// This code added to correctly implement the disposable pattern.
			public void Dispose()
			{
				// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
				Dispose(true);
				// TODO: uncomment the following line if the finalizer is overridden above.
				// GC.SuppressFinalize(this);
			}
			#endregion

			public string TempPath { get; protected set; }
			public string WhitePath { get; protected set; }
			public string BlackPath { get; protected set; }

			public int WhiteCount { get; protected set; }
			public int BlackCount { get; protected set; }
			public int GreyCount { get; protected set; }
			public int RedCount { get; protected set; }
			public int TotalCount
			{
				get
				{
					return WhiteCount + BlackCount + GreyCount + RedCount;
				}
			}

			protected void MakeRandomTempDir()
			{
				if (TempPath != null)
					ClearTempDirLayout();

				do
				{
					TempPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
				} while (Directory.Exists(TempPath));

				Directory.CreateDirectory(TempPath);
			}

			protected string MakeTempDirLayout()
			{
				MakeRandomTempDir();

				WhitePath = Directory.CreateDirectory(Path.Combine(TempPath, "white_files")).FullName;
				BlackPath = Directory.CreateDirectory(Path.Combine(TempPath, "black_files")).FullName;

				for (int i = 1; i <= 5; i++)
				{
					string content = $"This is the content for file {i}";

					File.WriteAllText(Path.Combine(WhitePath, $"white{i}.white"), content);
					File.WriteAllText(Path.Combine(TempPath, $"white{i}.white"), content);
					WhiteCount += 2;

					File.WriteAllText(Path.Combine(BlackPath, $"black{i}.black"), content);
					File.WriteAllText(Path.Combine(TempPath, $"black{i}.black"), content);
					BlackCount += 2;

					File.WriteAllText(Path.Combine(WhitePath, $"grey{i}.grey"), content);
					File.WriteAllText(Path.Combine(BlackPath, $"grey{i}.grey"), content);
					File.WriteAllText(Path.Combine(TempPath, $"grey{i}.grey"), content);
					GreyCount += 3;
				}

				return TempPath;
			}

			public void MutateTempDirLayout()
			{
				for (int i = 1; i <= 3; i++)
				{
					string content = $"This is the content for file {i}";

					File.Delete(Path.Combine(WhitePath, $"grey{i}.grey"));
					File.Delete(Path.Combine(BlackPath, $"grey{i}.grey"));
					File.Delete(Path.Combine(TempPath, $"grey{i}.grey"));
					GreyCount -= 3;

					File.WriteAllText(Path.Combine(WhitePath, $"red{i}.red"), content);
					File.WriteAllText(Path.Combine(BlackPath, $"red{i}.red"), content);
					File.WriteAllText(Path.Combine(TempPath, $"red{i}.red"), content);
					RedCount += 3;
				}
			}

			protected void ClearTempDirLayout()
			{
				Directory.Delete(TempPath, true);
				TempPath = null;
			}

		}

		[TestMethod()]
		public void FileLoader_LoadsFileDirectoryTreeOnCrawl()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path);
				reader.Crawl();
				Assert.IsTrue(reader.Files.Count == 35);
			}
		}

		[TestMethod()]
		public void FileLoader_FilesNotLoadedOnCrawl()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path);
				reader.Crawl();
				Assert.IsFalse(reader.Files.Any(x => x.Value != null));
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(UnauthorizedAccessException))]
		public void FileLoader_ThrowsExceptionIfDirectoryCannotLoad()
		{
			FileLoader reader = new FileLoader(@"C:\Windows\system32");
			reader.Crawl();
		}

		[TestMethod()]
		public void FileLoader_NoFilterLoadsEverything()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path)
				{
					FilterMode = ExtensionFilterMode.None
				};
				reader.Crawl();
				DirectoryInfo di = new DirectoryInfo(path);
				var files = di.GetFiles("*", SearchOption.AllDirectories);

				Assert.AreEqual(reader.Files.Count, sandbox.TotalCount);
			}
		}



		[TestMethod()]
		public void FileLoader_ReloadReflectsUpdatedDirectory()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path)
				{
					FilterMode = ExtensionFilterMode.None
				};
				reader.Crawl();

				sandbox.MutateTempDirLayout();
				reader.Crawl();

				DirectoryInfo di = new DirectoryInfo(path);
				var files = di.GetFiles("*", SearchOption.AllDirectories);

				Assert.AreEqual(reader.Files.Count, sandbox.TotalCount);
			}
		}

		[TestMethod()]
		[ExpectedException(typeof(FileNotFoundException))]
		public void FileLoader_LoadFilesThrowsExceptionIfFilesAreMissing()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path)
				{
					FilterMode = ExtensionFilterMode.None
				};
				reader.Crawl();

				sandbox.MutateTempDirLayout();

				reader.LoadAllFiles();
			}
		}

		[TestMethod()]
		public void FileLoader_LoadedFilesMatchesNumberOfCrawledFiles()
		{
			using (var sandbox = new TempDirectorySandbox())
			{
				string path = sandbox.TempPath;
				FileLoader reader = new FileLoader(path)
				{
					FilterMode = ExtensionFilterMode.None
				};
				reader.Crawl();

				reader.LoadAllFiles();

				Assert.AreEqual(sandbox.TotalCount, reader.Files.Count);
			}
		}

		[TestMethod()]
		public void FileLoader_NormalizePathRemovesBackslashes()
		{
			string result = FileLoader.NormalizePath(@"C:\test\test");
			Assert.IsFalse(result.Contains('\\'));
		}


		//[TestMethod()]
		//public void FileLoader_AddExtensionTest()
		//{
		//	Assert.Fail();
		//}

		//[TestMethod()]
		//public void FileLoader_RemoveExtensionTest()
		//{
		//	Assert.Fail();
		//}

		//[TestMethod()]
		//public void FileSystemReaderSystem_WhitelistOnlyLoadsWhitelist()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		string path = sandbox.TempPath;
		//		FileLoader reader = new FileLoader(path);
		//		reader.AddExtension(".white");
		//		reader.FilterMode = ExtensionFilterMode.Whitelist;
		//		reader.Crawl();

		//		Assert.AreEqual(sandbox.WhiteCount, reader.Files.Count);

		//		foreach (string file in reader.Files.Values)
		//		{
		//			string ext = Path.GetExtension(file);
		//			if (string.IsNullOrWhiteSpace(ext) || !reader.ExtensionFilter.Any(x => x == ext))
		//			{
		//				Assert.Fail();
		//			}
		//		}
		//	}
		//}

		//[TestMethod()]
		//public void FileSystemReaderSystem_BlacklistLoadsEverythingButBlacklist()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		string path = sandbox.TempPath;
		//		FileLoader reader = new FileLoader(path);
		//		reader.AddExtension(".black");
		//		reader.FilterMode = ExtensionFilterMode.Blacklist;
		//		reader.Crawl();

		//		Assert.AreEqual(sandbox.TotalCount - sandbox.BlackCount, reader.Files.Count);

		//		foreach (string file in reader.Files.Values)
		//		{
		//			string ext = Path.GetExtension(file);
		//			if (string.IsNullOrWhiteSpace(ext))
		//			{
		//				continue;
		//			}

		//			if (reader.ExtensionFilter.Any(x => x == ext))
		//			{
		//				Assert.Fail();
		//			}
		//		}
		//	}
		//}
	}
}