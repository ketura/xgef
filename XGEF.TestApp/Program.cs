﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using XGEF;
using XGEF.Core;
using XGEF.Core.Events;
using XGEF.Core.Networking;
using XGEF.Game.Stats;
using XGEF.Units;
using XGEF.Compiler;

namespace Testgame
{
	public class Program
	{
		static void Main(string[] args)
		{
			//XGEFSettings.WriteDefaultSettings("newsettings.json");
			SystemManager client = SystemLoader.SysManager;// (typeof(CoreStatSystem));

			try
			{
				//ScriptFile file = ScriptFile.AnalyzeFiles(File.ReadAllText("TestClass.cs")).First();
				//File.WriteAllText("output.cs", file.ReconstructCode());
				//File.WriteAllText("output2.cs", file.Text);
				//File.WriteAllText("output3.txt", file.GetDiagnostics());

				//string loop = "";
				//foreach(var node in file)
				//{
				//	loop += node.ToString() + "\n";
				//}
				//File.WriteAllText("output4.txt", loop);

				client.Info("\n\n\nBegin Execution:\n");

				client.Init();

				var network = client.GetSystem<CoreNetworkSystem>();
				var events = client.GetSystem<CoreEventSystem>();

				var serverAdapter = new ReliableIOServerAdapter("127.0.0.1", 1337);

				bool finished = false;
				serverAdapter.OnClientConnected +=
					(IClientAdapter c) =>
					{
						Console.WriteLine("Connection success!");
						Console.WriteLine("Server sending test message...");
						events.Invoke(null, new MessageEventArgs("Test message"));
					};

				events.GetEvent("MessageEvent").Subscribe((caller, arg) => { Console.WriteLine($"Recieved message in client: '{((MessageEventArgs)arg).Message}'"); finished = true; }, Priority.Normal);

				network.Connect("127.0.0.1", 1337);

				while(!finished)
				{
					client.Process();

				}

				//clientAdapter.Connect(serverAdapter);

				//var corestat = manager.GetSystem<CoreStatSystem>();

				//manager.Info($"Found {corestat.StatRegistry.Count()} stats.\n\n");
				//foreach (var stat in corestat.StatRegistry.Values)
				//{
				//	manager.Info(stat.Name);
				//}


				//var a = "A";
				//var b = "B";
				//var c = "C";
				//var d = "D";
				//var e = "E";
				//var f = "F";
				//var g = "G";


				//Graph<string> graph = new Graph<string>(new string[] { a, b, c, d, e, f, g });

				//graph.AddDirEdge(a, b);
				//graph.AddDirEdge(a, e);
				//graph.AddDirEdge(a, g);
				//graph.AddDirEdge(b, g);
				//graph.AddDirEdge(d, a);
				//graph.AddDirEdge(d, g);
				//graph.AddDirEdge(d, g);
				//graph.AddDirEdge(e, d);
				//graph.AddDirEdge(e, g);

				//graph.AddDirEdge(c, f);


				//var start = graph.GetStartNodes();
				//var end = graph.GetEndNodes();

				////var depth = graph.DepthSort();
				//var tarjan = graph.TarjanSort();
				////var sorted = graph.KahnSort();


				//var script = compiler.CompileText(code);

				//double d = 0.5;
				//object o = d;
				//Float01Stat stat = new Float01Stat("SomeStat", 0.3, "description", "notes");
				//stat.Set(o);
				//int val = 12;

				//NumericStat<int> test = new NumericStat<int>(val);

				//Species s = new Species("Charizard", "Great winged fire-breathing dragon.", "don't let this get weaker than any other species.");
				//s.AddBaseStat("HP", 210);
				//s.AddBaseStat("Endurance", 230);
				//s.AddBaseStat("Attack", 440);
				//s.AddBaseStat("Defense", 260);
				//s.AddBaseStat("SpecialAttack", 430);
				//s.AddBaseStat("SpecialDefense", 250);
				//s.AddBaseStat("Speed", 260);

				//StatContainer<Float01Stat> stats = new StatContainer<Float01Stat>("IVs", 0.1, "Shows the level of the {name}.", "ATK", "DEF", "SPATK", "SPDEF", "SPD", "HP");

				//List<Species> list = new List<Species>();
				//list.Add(s);
				//list.Add(s);
				//list.Add(s);
				//string json = JsonSerializer.SerializeJson(list);
				//File.WriteAllText("test.json", json);

				//List<Species> s2 = JsonSerializer.DeserializeJson<List<Species>>(File.ReadAllText("mon.json"));

				//Unit u = new Unit(s2);

				Console.WriteLine("\n\nOutput complete.  Press any key to exit.");
				Console.ReadKey();
			}
			catch (Exception ex)
			{
				client.Error("Error while running program.", ex);
				Console.ReadLine();
			}

		}
	}
}