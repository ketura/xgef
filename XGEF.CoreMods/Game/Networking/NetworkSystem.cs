
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using XGEF;

using XGEF.Core.Networking;

namespace XGEF.Game.Networking
{
	public partial class NetworkSystem : ModNetworkSystemBase
	{
		public NetworkSystem() : base()
		{
		}

		public override void PreInit() { }
		public override void Init()
		{
			Console.WriteLine("in the modded network system");

			if(!CoreSystem.IsServer)
			{
				SetClient(new ReliableIOClientAdapter());
				//SetClient(new NetcodeIOClientAdapter());

			}
			else
			{
				SetServer(new ReliableIOServerAdapter());
				//SetServer(new NetcodeIOServerAdapter());
			}
			//throw new NotImplementedException();
		}
		public override void PostInit() { }

		public override void PreProcess() { }
		public override void Process()
		{
			//throw new NotImplementedException();
		}
		public override void PostProcess() { }

		
	}
}
