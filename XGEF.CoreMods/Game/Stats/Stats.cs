﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Game.Stats;

namespace XGEF.Game.Stats
{
	public class StartingStats
	/* core pre*/{
		public int Test { get; }
		public enum Numbers { One, Two, Three=2 }
		#region SpeciesStats
		public static List<StatInfo> SpeciesStats;

		static StartingStats()
		{
			SpeciesStats = new List<StatInfo>()
			{
				new StatInfo()
				{
					Name = "Hit Points",
					Abbreviation = "HP",
					Description = "",
					Notes = "A pain threshold that regenerates relatively quickly.  Injuries are more likely the less HP one has.",
					MinValue = 0,
					MaxValue = 1000,
					DataType = typeof(short),
					ModName = Constants.CoreModName
				},
				#region stats
				//new StatInfo()
				//{
				//	Name = "Endurance",
				//	Abbreviation = "END",
				//	Description = "",
				//	Notes = "expended by using moves, movement, enduring hits, status effects, and others.  Regenerates slowly.  Is the primary form of measuring multi-battle readiness.  At 0 the creature faints.",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Attack",
				//	Abbreviation = "ATK",
				//	Description = "",
				//	Notes = "the physical strength and prowess of a creature.  Physical moves tend to scale off of this stat",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Defense",
				//	Abbreviation = "DEF",
				//	Description = "",
				//	Notes = "the durability of the creature.  All incoming damage is modified by this stat.",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Special Attack",
				//	Abbreviation = "SPATK",
				//	Description = "",
				//	Notes = "mental discipline and control of the elements.  Modified non-physical moves.",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Special Defense",
				//	Abbreviation = "SPDEF",
				//	Description = "",
				//	Notes = "fortitude and constitution.  This reduces or eliminates the chance and effect of negative status effects and mental attacks.",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Movement",
				//	Abbreviation = "MOV",
				//	Description = "",
				//	Notes = "how far the creature can move in a turn",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Speed",
				//	Abbreviation = "SPD",
				//	Description = "",
				//	Notes = "how frequently or effectively the unit takes their turn",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Critical",
				//	Abbreviation = "CRIT",
				//	Description = "",
				//	Notes = "how frequently the unit’s moves tend to hit super effectively",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Obedience",
				//	Abbreviation = "OBED",
				//	Description = "",
				//	Notes = "how loyal or strong-willed a species tends to be",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Trust",
				//	Abbreviation = "TRU",
				//	Description = "",
				//	Notes = "how friendly or distrustful a species tends to be",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Intelligence",
				//	Abbreviation = "INT",
				//	Description = "",
				//	Notes = "a modifier on how many moves a creature can learn, the speed of training, and other such factors",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Temperament",
				//	Abbreviation = "TEMP",
				//	Description = "",
				//	Notes = "how aggressive a species tends to be",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Accuracy",
				//	Abbreviation = "ACC",
				//	Description = "",
				//	Notes = "Modifier to the accuracy of a unit’s moves",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Evasion",
				//	Abbreviation = "EVA",
				//	Description = "",
				//	Notes = "Modifier to the dodge chance of a unit",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Resistance",
				//	Abbreviation = "RES",
				//	Description = "",
				//	Notes = "modifier to a unit’s type resistances",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Weakness",
				//	Abbreviation = "WEAK",
				//	Description = "",
				//	Notes = "modifier to a unit’s type weaknesses",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Height",
				//	Abbreviation = "HT",
				//	Description = "",
				//	Notes = "size of the unit",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Weight",
				//	Abbreviation = "WT",
				//	Description = "",
				//	Notes = "weight of the unit",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Perception",
				//	Abbreviation = "PER",
				//	Description = "",
				//	Notes = "ability to detect through senses other than sight",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Sight Cone",
				//	Abbreviation = "SIGHT",
				//	Description = "",
				//	Notes = "how far the unit can see",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Lifespan",
				//	Abbreviation = "LIFE",
				//	Description = "",
				//	Notes = "how long the species tends to live",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Breeding Age",
				//	Abbreviation = "BREED",
				//	Description = "",
				//	Notes = "how old a species unit must be to breed",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Aspect Affinity",
				//	Abbreviation = "A-AFFN",
				//	Description = "",
				//	Notes = "modifier to the effectiveness of learning/using a particular aspect",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Move Affinity",
				//	Abbreviation = "M-AFFN",
				//	Description = "",
				//	Notes = "modifier to the effectiveness of learning/using a particular move",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//},
				//new StatInfo()
				//{
				//	Name = "Diet",
				//	Abbreviation = "",
				//	Description = "",
				//	Notes = "A list of things the species can safely eat",
				//	MinValue = 0,
				//	MaxValue = 1000,
				//	DataType = typeof(short),
				//	ModName = Constants.CoreModName
				//}
				#endregion
			};

			int test = 0;
		}
		#endregion


	}/* core post*/
}
