﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;

namespace XGEF.Compiler
{
	//fix namespace to be correct for all core files
	//files with no namespace have one added
	//
	public class Analyzer
	{
		public List<Transformer> Transforms;
		protected List<SyntaxTree> trees;
		protected List<ScriptFile> scripts;

		public string Analyze(string filename, params string[] files)
		{
			return Analyze(filename, files.ToList());
		}

		public string Analyze(string filename, IEnumerable<string> files)
		{
			trees = new List<SyntaxTree>();
			scripts = new List<ScriptFile>();
			foreach(string file in files)
			{
				trees.Add(CSharpSyntaxTree.ParseText(file));
				scripts.Add(ScriptFile.AnalyzeFile(filename, file));

				foreach (var transformer in Transforms)
				{
					scripts = transformer.Transform(scripts).ToList();
				}
			}

			return ScriptFile.FilterTextBasic(scripts.Last().ReconstructCode());
		}

		public void AddTransformer(Transformer trans)
		{
			Transforms.Add(trans);
		}

		public void AddMods(IEnumerable<string> mods)
		{
			if (mods == null || mods.Count() == 0)
				return;

			foreach(var trans in Transforms)
			{
				if (trans.LoadedMods == null)
					trans.LoadedMods = mods.ToList();
				else
					trans.LoadedMods.AddRange(mods);
			}
		}

		public void AddTransformers(params Transformer[] transformers)
		{
			if (transformers == null)
				return;

			foreach (var trans in transformers)
				AddTransformer(trans);
		}

		public static Analyzer StandardAnalyzer()
		{
			return new Analyzer(
				new EmptyMemberContainerTransformer(),
				new RequiresModTransformer(),
				new PrependFunctionTransformer(),
				new AppendFunctionTransformer(),
				new EncloseFunctionTransformer(),
				new ExplicitOverwriteTransformer(false),
				new OverwriteTransformer(),
				new ExtendTransformer(),

				new EmptyMemberContainerTransformer()
				//new ExplicitOverwriteTransformer(true)
				);
		}

		public Analyzer() : this(null) { }
		public Analyzer(params Transformer[] transforms)
		{
			Transforms = new List<Transformer>();
			AddTransformers(transforms);
		}

	}
}
