﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;

using System.Text.RegularExpressions;
using System.Collections;
using System.Diagnostics;

namespace XGEF.Compiler
{
	[DebuggerDisplay("ID:{ID} {Text}")]
	public partial class ScriptFile : IEnumerable<INode>
	{
		private static int IDPool;

		public string OriginalText { get; set; }
		public string Text { get { return ReconstructCode(); } }
		public Dictionary<string, INode> TopLevel { get; set; }
		public List<Using> Usings { get; set; }
		public int ID { get; }

		public int Count { get; protected set; }

		public void AddMember(INode node)
		{
			TopLevel[node.FullName] = node;
			node.Parent = null;
			node.ScriptFile = this;
			foreach (var child in node.Children())
				node.ScriptFile = this;
			Count++;
		}

		public void InsertMember(INode node, string parentName)
		{
			INode parent = MemberWithFullName(parentName);
			if (parent == null)
			{
				AddMember(node);
			}
			else
			{
				node.Parent = parent;
				parent.AddChild(node);
				node.ScriptFile = this;
				foreach (var child in node.Children())
					child.ScriptFile = this;
			}
		}

		public bool RemoveMember(INode node)
		{
			if (TopLevel.ContainsValue(node))
			{
				node.Parent = null;
				Count--;
				return TopLevel.Remove(node.FullName);
			}

			foreach(var member in Members())
			{
				bool result = member.RemoveChild(node);
				if (result)
				{
					node.Parent = null;
					Count--;
					return true;
				}
			}

			return false;
		}

		public string ReconstructCode()
		{
			string result = "";
			if (Usings != null && Usings.Count > 0)
				result += GetAllUsings();
			foreach (INode node in TopLevel.Values)
			{
				result += node.ReconstructCode();
			}
			return result;
		}

		public string GetAllUsings()
		{
			return string.Join("", Usings.Select(x => x.Declaration).ToArray());
		}

		public string GetDiagnostics()
		{
			string result = GetAllUsings();

			foreach (Namespace name in TopLevel.Values)
			{
				result += name.GetDiagnostics();
			}
			return result;
		}

		public INode MemberWithFullName(string fullname)
		{
			return Members().ToList().Where(x => x.FullName == fullname).FirstOrDefault();
		}

		public IEnumerable<INode> MembersWithName(string name)
		{
			return Members().ToList().Where(x => x.Name == name);
		}

		public IEnumerable<INode> MembersOfType(MemberType type)
		{
			return Members().ToList().Where(x => x.Type == type);
		}

		public IEnumerable<INode> MembersWithSignature(INode node)
		{
			return Members().ToList().Where(x => x.CompareSignature(node));
		}

		public INode FindExactMember(INode node)
		{
			return Members().ToList().Where(x => x.CompareFullName(node) && x.CompareSignature(node)).FirstOrDefault();
		}

		public IEnumerable<INode> MembersWithAttributes()
		{
			return Members().ToList().Where(x => x.HasAttributes);
		}

		public IEnumerable<INode> MembersWithAttribute(string name)
		{
			return Members().ToList().Where(x => x.HasAttribute(name));
		}

		public virtual IEnumerable<INode> Members()
		{
			foreach (INode node in TopLevel.Values)
			{
				yield return node;
				foreach (INode value in node.Descendants())
					yield return value;
			}
		}

		public IEnumerator<INode> GetEnumerator()
		{
			foreach (INode node in Members())
			{
				yield return node;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public ScriptFile()
		{
			TopLevel = new Dictionary<string, INode>();
			Usings = new List<Using>();
			ID = IDPool++;
		}
	}

	public interface INode : IEnumerable<INode>
	{
		ScriptFile ScriptFile { get; set; }
		string Filename { get; set; }
		string Name { get; set; }
		INode Parent { get; set; }
		MemberType Type { get; set; }
		string OriginalText { get; set; }
		List<string> Modifiers { get; set; }
		string GetModifiers();
		string Indent { get; set; }

		string FullName { get; }
		string Declaration { get; }
		string Text { get; }
		string FullBody { get; }
		string Body { get; set; }

		void AddChild(INode node);

		List<Attribute> Attributes { get; }
		bool HasAttributes { get; }
		bool HasAttribute(string name);
		Attribute GetAttribute(string name);
		string GetAttributesCode();
		IEnumerable<Attribute> GetAttributes(string name);
		bool IsExpression { get; set; }

		T GetParent<T>() where T : class, INode;

		string Surround(string body);
		string ReconstructCode();
		string GetDiagnostics(int tabs);
		IEnumerable<INode> Descendants();
		IEnumerable<INode> Children();
		bool RemoveChild(INode node);
		bool CompareSignature(INode node);
		bool CompareName(INode node);
		bool CompareFullName(INode node);
		bool CompareMembers(INode node);
		bool CompareTo(INode node);
		bool IsComparable(INode node);
		bool IsCompatible(INode node);
	}

	public abstract partial class Node : INode
	{
		public static string GetArgumentList(IEnumerable<string> args, string def = "()")
		{
			string result = def;
			if (args.Count() > 0)
			{
				result = $"({string.Join(",", args.ToArray())})";
			}
			return result;
		}

		public static string GetParameterList(IEnumerable<KeyValuePair<string, string>> args, string def = "()")
		{
			string result = def;
			if (args.Count() > 0)
			{
				result = $"({string.Join(", ", args.Select(x => x.Key + " " + x.Value).ToArray())})";
			}
			return result;
		}

		public static string SpaceOrNull(string str)
		{
			if (String.IsNullOrWhiteSpace(str))
				return "";

			return $"{str + " "}";
		}

		public virtual string Filename { get; set; }
		public virtual string Name { get; set; }
		public virtual string FullName { get { return $"{(Parent?.FullName == null ? "" : Parent.FullName + ".")}{Name}"; } }
		public virtual INode Parent { get; set; }
		public virtual void AddChild(INode node) { }
		public ScriptFile ScriptFile { get; set; }
		public MemberType Type { get; set; }
		public List<Attribute> Attributes { get; set; }
		public virtual bool HasAttributes { get { return Attributes.Count > 0; } }
		public virtual bool HasAttribute(string name)
		{
			return Attributes.Any(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute"));
		}
		public virtual Attribute GetAttribute(string name)
		{
			return Attributes.Where(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute")).FirstOrDefault();
		}

		public virtual IEnumerable<Attribute> GetAttributes(string name)
		{
			return Attributes.Where(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute"));
		}
		public virtual void AddAttribute(Attribute attrib) { Attributes.Add(attrib); }
		public bool IsExpression { get; set; }

		protected virtual List<Type> ValidParents { get; }
		public virtual T GetParent<T>()
			where T : class, INode
		{
			if (ValidParents.Contains(typeof(T)))
				return Parent as T;

			return null;
		}

		public virtual string GetModifiers()
		{
			return String.Join(" ", Modifiers.ToArray());
		}

		public virtual string GetAttributesCode()
		{
			return String.Join("\n", Attributes.Select(x => x.Text));
		}

		//the original unmodified script that defines the entire declaration + body
		public virtual string OriginalText { get; set; }
		//protection level, overrides, declaration term, etc.
		public virtual List<string> Modifiers { get; set; }
		//how far the declaration is indented relative to the parent (usually one tab)
		public virtual string Indent { get; set; }
		//modifiers + name + arguments, basically everything that defines the signature
		public virtual string Declaration
		{
			get
			{
				return $"{SpaceOrNull(GetModifiers())}{Name}";
			}
		}

		//fully formatted and cleaned up version of the code
		public virtual string Text
		{ get
			{
				return $"{GetAttributesCode()}{Declaration}{FullBody}";
			}
		}
		//The content of the body, not counting elements such as surrounding curly braces and with a normalized indentation
		public virtual string Body { get; set; }
		//The body with the proper indentation and braces
		public virtual string FullBody { get { return Surround(Body); } }
		public virtual string Surround(string body) { return Body; }

		public Node()
		{
			ValidParents = new List<Type>();
			Attributes = new List<Attribute>();
			Modifiers = new List<string>();
		}

		public virtual string ReconstructCode()
		{
			return Text;
		}

		public virtual string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			return $"{result}{Type}: {FullName}\n";
		}

		public virtual IEnumerator<INode> GetEnumerator()
		{
			foreach (var child in Children())
			{
				yield return child;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public virtual IEnumerable<INode> Descendants()
		{
			foreach (INode node in Children())
			{
				yield return node;
				foreach (INode value in node.Descendants())
					yield return value;
			}
		}

		public virtual IEnumerable<INode> Children()
		{
			yield break;
		}

		public virtual bool RemoveChild(INode node) { return false; }

		public override string ToString()
		{
			return $"{Type}:{FullName}";
		}
	}

	public abstract class BlockNode : Node
	{
		public override string Text
		{
			get
			{
				return $"{GetAttributesCode()}{Declaration}{FullBody}";
			}
		}

		public override string Declaration
		{
			get
			{
				return $"{SpaceOrNull(GetModifiers())}{Name}";
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (IsExpression)
			{
				result = $" => {body};\n";
			}
			else
			{
				if (String.IsNullOrWhiteSpace(body))
				{
					result = " { }";
				}
				else
				{
					result = $"\n{{\n{Regex.Replace(body, @"^", Indent, RegexOptions.Multiline)}\n}}";
				}
			}
			
			return result;
		}
	}

	public abstract class ContainerNode : BlockNode
	{
		public override string Body
		{
			get
			{
				string result = "";
				foreach (INode node in this)
				{
					result += $"{node.Text}\n";
				}

				return result.TrimEnd('\n');
			}
		}
	}

	public partial class Using : Node
	{
		public override string Declaration { get { return $"using {Name};\n"; } }
	}


	public partial class Namespace : ContainerNode
	{
		public Namespace GetParent() { return Parent as Namespace; }
		
		public Dictionary<string, Class> Classes { get; }
		public Dictionary<string, Enum> Enums { get; }
		public Dictionary<string, Class> Structs { get; }
		public Dictionary<string, Delegate> Delegates { get; }
		public Dictionary<string, Class> Interfaces { get; }
		public Dictionary<string, Namespace> Namespaces { get; }

		public void AddClass(Class node) { Classes[node.FullName] = node; }
		public void AddEnum(Enum node) { Enums[node.FullName] = node; }
		public void AddStruct(Class node) { Structs[node.FullName] = node; }
		public void AddDelegate(Delegate node) { Delegates[node.FullName] = node; }
		public void AddInterface(Class node) { Interfaces[node.FullName] = node; }
		public void AddNamespace(Namespace node) { Namespaces[node.FullName] = node; }

		public override void AddChild(INode node)
		{
			switch (node.Type)
			{
				case MemberType.Class:
					AddClass(node as Class);
					break;
				case MemberType.Struct:
					AddStruct(node as Class);
					break;
				case MemberType.Interface:
					AddInterface(node as Class);
					break;
				case MemberType.Enum:
					AddEnum(node as Enum);
					break;
				case MemberType.Delegate:
					AddDelegate(node as Delegate);
					break;
				case MemberType.Namespace:
					AddNamespace(node as Namespace);
					break;
				case MemberType.Attribute:
					AddAttribute(node as Attribute);
					break;
			}
		}

		public override IEnumerable<INode> Children()
		{
			foreach (INode node in Namespaces.Values)
				yield return node;

			foreach (INode node in Enums.Values)
				yield return node;

			foreach (INode node in Delegates.Values)
				yield return node;

			foreach (INode node in Interfaces.Values)
				yield return node;

			foreach (INode node in Classes.Values)
				yield return node;

			foreach (INode node in Structs.Values)
				yield return node;
		}

		public override bool RemoveChild(INode node)
		{
			if (Namespaces.ContainsKey(node.FullName))
				return Namespaces.Remove(node.FullName);

			if (Enums.ContainsKey(node.FullName))
				return Enums.Remove(node.FullName);

			if (Delegates.ContainsKey(node.FullName))
				return Delegates.Remove(node.FullName);

			if (Interfaces.ContainsKey(node.FullName))
				return Interfaces.Remove(node.FullName);

			if (Classes.ContainsKey(node.FullName))
				return Classes.Remove(node.FullName);

			if (Structs.ContainsKey(node.FullName))
				return Structs.Remove(node.FullName);

			return false;
		}

		public override string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			result += $"{Type}: {FullName}:\n";

			foreach (INode node in Children())
			{
				result += node.GetDiagnostics(tabs + 1);
			}

			return result;
		}

		public Namespace()
		{
			Classes = new Dictionary<string, Class>();
			Enums = new Dictionary<string, Enum>();
			Structs = new Dictionary<string, Class>();
			Delegates = new Dictionary<string, Delegate>();
			Interfaces = new Dictionary<string, Class>();
			Namespaces = new Dictionary<string, Namespace>();
			ValidParents.AddRange(new[] { typeof(Namespace) } );
		}
	}

	public partial class Class : ContainerNode
	{
		public List<string> BaseClasses { get; set; }

		public override string Declaration
		{
			get
			{
				string inherit = "";
				if(BaseClasses.Count() > 0)
				{
					inherit = $" : {string.Join(",", BaseClasses.ToArray())}";
				}
				return $"{SpaceOrNull(GetModifiers())}{Name}{inherit}";
			}
		}

		public Dictionary<string, Method> Constructors { get; }
		public Dictionary<string, Field> Constants { get; }
		public Dictionary<string, Field> Fields { get; }
		public Dictionary<string, Method> Finalizers { get; }
		public Dictionary<string, Method> Methods { get; }
		public Dictionary<string, Property> Properties { get; }
		public Dictionary<string, Property> Indexers { get; }
		public Dictionary<string, Method> Operators { get; }
		public Dictionary<string, Field> Events { get; }
		public Dictionary<string, Delegate> Delegates { get; }
		public Dictionary<string, Class> Classes { get; }
		public Dictionary<string, Enum> Enums { get; }
		public Dictionary<string, Class> Structs { get; }
		public Dictionary<string, Class> Interfaces { get; }
		
		
		public void AddEnum(Enum node) { Enums[node.FullName] = node; }
		public void AddInterface(Class node) { Interfaces[node.FullName] = node; }
		public void AddClass(Class node) { Classes[node.FullName] = node; }
		public void AddStruct(Class node) { Structs[node.FullName] = node; }
		public void AddDelegate(Delegate node) { Delegates[node.FullName] = node; }
		public void AddEvent(Field node) { Events[node.FullName] = node; }
		public void AddConstant(Field node) { Constants[node.FullName] = node; }
		public void AddField(Field node) { Fields[node.FullName] = node; }
		public void AddProperty(Property node) { Properties[node.FullName] = node; }
		public void AddIndexer(Property node) { Indexers[node.FullName] = node; }
		public void AddOperator(Method node) { Operators[node.FullName] = node; }
		public void AddConstructor(Method node)
		{
			//Done differently on purpose; any constructors are messily stored since parsing them down to ObjectName(int, int) or equivalent
			// is more difficult than the utility gained.
			Constructors[node.Declaration] = node;
		}
		public void AddFinalizer(Method node) { Finalizers[node.FullName] = node; }
		public void AddMethod(Method node) { Methods[node.Declaration] = node; }

		public override void AddChild(INode node)
		{
			switch (node.Type)
			{
				case MemberType.Class:
					AddClass(node as Class);
					break;
				case MemberType.Struct:
					AddStruct(node as Class);
					break;
				case MemberType.Interface:
					AddInterface(node as Class);
					break;
				case MemberType.Enum:
					AddEnum(node as Enum);
					break;
				case MemberType.Delegate:
					AddDelegate(node as Delegate);
					break;
				case MemberType.Event:
					AddEvent(node as Field);
					break;
				case MemberType.Method:
					AddMethod(node as Method);
					break;
				case MemberType.Constructor:
					AddConstructor(node as Method);
					break;
				case MemberType.Finalizer:
					AddFinalizer(node as Method);
					break;
				case MemberType.Operator:
					AddOperator(node as Method);
					break;
				case MemberType.Constant:
					AddConstant(node as Field);
					break;
				case MemberType.Field:
					AddField(node as Field);
					break;
				case MemberType.Property:
					AddProperty(node as Property);
					break;
				case MemberType.Indexer:
					AddIndexer(node as Property);
					break;
				case MemberType.Attribute:
					AddAttribute(node as Attribute);
					break;
			}
		}

		public override IEnumerable<INode> Children()
		{
			foreach (INode node in Enums.Values)
				yield return node;

			foreach (INode node in Interfaces.Values)
				yield return node;

			foreach (INode node in Classes.Values)
				yield return node;

			foreach (INode node in Structs.Values)
				yield return node;

			foreach (INode node in Delegates.Values)
				yield return node;

			foreach (INode node in Events.Values)
				yield return node;

			foreach (INode node in Constants.Values)
				yield return node;

			foreach (INode node in Fields.Values)
				yield return node;

			foreach (INode node in Properties.Values)
				yield return node;

			foreach (INode node in Indexers.Values)
				yield return node;

			foreach (INode node in Operators.Values)
				yield return node;

			foreach (INode node in Constructors.Values)
				yield return node;

			foreach (INode node in Finalizers.Values)
				yield return node;

			foreach (INode node in Methods.Values)
				yield return node;
		}

		public override bool RemoveChild(INode node)
		{
			if (Enums.ContainsKey(node.FullName))
				return Enums.Remove(node.FullName);
			if (Interfaces.ContainsKey(node.FullName))
				return Interfaces.Remove(node.FullName);
			if (Classes.ContainsKey(node.FullName))
				return Classes.Remove(node.FullName);
			if (Structs.ContainsKey(node.FullName))
				return Structs.Remove(node.FullName);
			if (Delegates.ContainsKey(node.FullName))
				return Delegates.Remove(node.FullName);
			if (Events.ContainsKey(node.FullName))
				return Events.Remove(node.FullName);
			if (Constants.ContainsKey(node.FullName))
				return Constants.Remove(node.FullName);
			if (Fields.ContainsKey(node.FullName))
				return Fields.Remove(node.FullName);
			if (Properties.ContainsKey(node.FullName))
				return Properties.Remove(node.FullName);
			if (Indexers.ContainsKey(node.FullName))
				return Indexers.Remove(node.FullName);
			if (Operators.ContainsKey(node.FullName))
				return Operators.Remove(node.FullName);
			if (Methods.ContainsKey(node.Declaration))
				return Methods.Remove(node.Declaration);
			//remember that constructors are wonky
			if (Constructors.ContainsKey(node.Declaration))
				return Constructors.Remove(node.Declaration);
			if (Finalizers.ContainsKey(node.FullName))
				return Finalizers.Remove(node.FullName);

			return false;
		}

		public override string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			result += $"{Type}: {FullName}:\n";

			foreach (INode node in this)
			{
				result += node.GetDiagnostics(tabs + 1);
			}

			return result;
		}

		public Class()
		{
			Constructors = new Dictionary<string, Method>();
			Constants = new Dictionary<string, Field>();
			Fields = new Dictionary<string, Field>();
			Finalizers = new Dictionary<string, Method>();
			Methods = new Dictionary<string, Method>();
			Properties = new Dictionary<string, Property>();
			Indexers = new Dictionary<string, Property>();
			Operators = new Dictionary<string, Method>();
			Events = new Dictionary<string, Field>();
			Delegates = new Dictionary<string, Delegate>();
			Classes = new Dictionary<string, Class>();
			Enums = new Dictionary<string, Enum>();
			Structs = new Dictionary<string, Class>();
			Interfaces = new Dictionary<string, Class>();
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class) });

			BaseClasses = new List<string>();
		}
	}

	public partial class Enum : BlockNode
	{
		public Dictionary<string, string> Values { get; set; }

		public override string Body
		{
			get
			{
				var pairs = Values.Select(x => string.Join("", x.Key, x.Value));
				return string.Join(", ", pairs.ToArray());
			}
		}

		public override string Declaration
		{
			get
			{
				return $"{SpaceOrNull(GetModifiers())}enum {Name}";
			}
		}

		public Enum()
		{
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class) });
			Values = new Dictionary<string, string>();
		}
	}

	public partial class Attribute : Node
	{
		public List<string> Arguments { get; set; }

		public override string Text
		{
			get
			{
				return $"[{Declaration}{GetArgumentList(Arguments, "")}]\n";
			}
		}

		public Attribute()
		{
			Arguments = new List<string>();
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class), typeof(Field), typeof(Method),
				typeof(Enum), typeof(Property),});
		}
	}

	public partial class Method : BlockNode
	{
		public List<string> ReturnTypes { get; set; }
		public List<KeyValuePair<string, string>> Parameters { get; set; }
		public string Initializer { get; set; }

		public override string Surround(string body)
		{
			string result = "";

			if (IsExpression)
			{
				result = $"{Initializer ?? ""} => {body};";
			}
			else if(body != null && body.Equals(";"))
			{
				result = $";";
			}
			else
			{ 
				if (String.IsNullOrWhiteSpace(body))
				{
					result = $"{Initializer ?? ""} {{ }}";
				}
				else
				{
					result = $"{Initializer ?? ""}\n{{\n{Regex.Replace(body, @"^", Indent, RegexOptions.Multiline)}\n}}";
				}
			}

			return result;
		}

		public override string Declaration
		{
			get
			{
				string returns = "";
				if (ReturnTypes.Count() > 0)
				{
					returns = $"{string.Join(", ", ReturnTypes.ToArray())}";
				}
				if(ReturnTypes.Count() > 1)
				{
					returns = $"({returns})";
				}

				return $"{SpaceOrNull(GetModifiers())}{SpaceOrNull(returns)}{Name}{GetParameterList(Parameters)}";
			}
		}

		public Method()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
			ReturnTypes = new List<string>();
			Parameters = new List<KeyValuePair<string, string>>();
		}
	}

	public partial class Field : Node
	{
		public bool Constant { get; set; }
		public string VariableType { get; set; }

		public override string Text
		{
			get
			{
				return $"{Declaration};";
			}
		}

		public override string Declaration
		{
			get
			{
				return $"{SpaceOrNull(GetModifiers())}{VariableType} {Name}";
			}
		}

		public Field()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
		}
	}

	public partial class Delegate : Node
	{
		public string ReturnType { get; set; }
		public List<KeyValuePair<string, string>> Parameters { get; set; }

		public override string Text
		{
			get
			{
				return $"{Declaration}{GetParameterList(Parameters)};";
			}
		}

		public override string Declaration
		{
			get
			{
				return $"{SpaceOrNull(GetModifiers())}delegate {SpaceOrNull(ReturnType)}{Name}";
			}
		}


		public Delegate()
		{
			Parameters = new List<KeyValuePair<string, string>>();
			ValidParents.AddRange(new[] { typeof(Class) });
		}
	}

	public partial class Property : BlockNode
	{
		public string FieldType { get; set; }

		public string IndexerType { get; set; }
		public string IndexerArg { get; set; }

		public string GetBody { get; set; }
		public bool GetExpression { get; set; }
		public string SetBody { get; set; }
		public bool SetExpression { get; set; }

		public bool OneLine { get { return IsExpression || (String.IsNullOrEmpty(GetBody) && String.IsNullOrEmpty(SetBody)); } }

		private string _body;
		public override string Body
		{
			get
			{
				string result = "";

				if (IsExpression)
				{
					result = _body;
				}
				else
				{
					if(GetBody != null)
					{
						if (GetBody.Equals(""))
						{
							result += "get;";
							if(OneLine)
							{
								if(SetBody != null)
									result += " ";
							}
							else
							{
								result += "/n";
							}
						}
						else if (GetExpression)
						{
							result += $"get => {GetBody};\n";
						}
						else
						{ 
							result += $"get\n{{\n{Indent}{GetBody}\n}}\n";
						}
					}

					if (SetBody != null)
					{
						if (SetBody.Equals(""))
						{
							result += "set;";
							if (!OneLine)
							{
								result += "/n";
							}
						}
						else if (SetExpression)
						{
							result += $"set => {SetBody};\n";
						}
						else
						{
							result += $"set\n{{\n{Indent}{SetBody}\n}}\n";
						}
					}
				}

				return result.TrimEnd('\n');
			}

			set
			{
				_body = value;
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (IsExpression)
			{
				result = $" => {body};";
			}
			else if(OneLine)
			{
				result = $" {{ {body} }}";
			}
			else
			{
				if (String.IsNullOrWhiteSpace(body))
				{
					result = " { }";
				}
				else
				{
					result = $"\n{{\n{Regex.Replace(body, @"^", Indent, RegexOptions.Multiline)}\n}}";
				}
			}

			return result;
		}

		public override string Declaration
		{
			get
			{
				if(Type == MemberType.Indexer)
				{
					return $"{SpaceOrNull(GetModifiers())}{SpaceOrNull(FieldType)}this[{IndexerType} {IndexerArg}]";
				}

				return $"{SpaceOrNull(GetModifiers())}{SpaceOrNull(FieldType)}{Name}";
			}
		}

		public Property()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
		}
	}

	
}
