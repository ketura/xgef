﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Text.RegularExpressions;

namespace XGEF.Compiler
{

	public partial class ScriptFile
	{
		public static string DefaultNamespace = "DefaultNamespace";

		//These aren't overloads of the Tree versions because else you need to include references
		// to Roslyn to use it, even if you don't access the overloaded version that requires a SyntaxTree
		public static ScriptFile AnalyzeFile(string relativeFilename, string content)
		{
			string filtered = FilterTextBasic(content);
			var file = AnalyzeTree(relativeFilename, CSharpSyntaxTree.ParseText(filtered));
			file.OriginalText = content;
			return file;
		}

		public static IEnumerable<ScriptFile> AnalyzeFiles(string relativeFilename, params string[] files)
		{
			var Files = new List<ScriptFile>();

			foreach (var file in files)
			{
				Files.Add(AnalyzeFile(relativeFilename, file));
			}

			return Files;
		}

		public static string FilterTextComplete(string script)
		{
			script = FilterTextBasic(script);
			script = Regex.Replace(script, @"\n", "");
			script = Regex.Replace(script, @"\t", "");
			script = Regex.Replace(script, @"  +", " ");

			return script;
		}

		public static string FilterTextBasic(string script)
		{
			//stinking windows line endings
			script = Regex.Replace(script, @"\r", "");
			//C-style comments
			script = Regex.Replace(script, @"//.*", "");
			//block comments
			script = Regex.Replace(script, @"/\*.*?\*/", "", RegexOptions.Singleline);
			//region start
			script = Regex.Replace(script, @"#region.*", "");
			//region end
			script = Regex.Replace(script, @"#endregion.*", "");
			//any large whitespace
			script = Regex.Replace(script, @"\n(\s*\n)+", "\n", RegexOptions.Singleline);

			return script;
		}

		public static ScriptFile AnalyzeTree(string filename, SyntaxTree tree)
		{
			var root = tree.GetCompilationUnitRoot();
			if (root == null)
				throw new Exception("somehow this tree ain't got no compilation unit");

			return ParseRoot(root);
		}

		public static IEnumerable<ScriptFile> AnalyzeTrees(string filename, List<SyntaxTree> trees)
		{
			var Files = new List<ScriptFile>();

			foreach (var tree in trees)
			{
				Files.Add(AnalyzeTree(filename, tree));
			}

			return Files;
		}

		private static string GetSection(SyntaxTree tree, int startPos, int endPos)
		{
			//return tree.GetText().GetSubText(new TextSpan(startPos, endPos)).ToString();
			return tree.GetText().ToString().Substring(startPos, endPos - startPos);
		}

		private static ScriptFile ParseRoot(CompilationUnitSyntax root)
		{
			ScriptFile file = new ScriptFile
			{
				OriginalText = root.ToFullString()
			};

			var children = root.ChildNodes();

			foreach (var member in children)
			{
				switch (member)
				{
					case UsingDirectiveSyntax x:
						file.Usings.Add(ParseUsing(x));
						break;

					case NamespaceDeclarationSyntax x:
						file.AddMember(ParseNamespace(x, null));
						break;

					case ClassDeclarationSyntax x:
						file.AddMember(ParseClass(x, null, MemberType.Class));
						break;

					case InterfaceDeclarationSyntax x:
						file.AddMember(ParseClass(x, null, MemberType.Interface));
						break;

					case StructDeclarationSyntax x:
						file.AddMember(ParseClass(x, null, MemberType.Struct));
						break;

					case DelegateDeclarationSyntax x:
						file.AddMember(ParseDelegate(x, null, MemberType.Delegate));
						break;

					case EnumDeclarationSyntax x:
						file.AddMember(ParseEnum(x, null));
						break;
				}
			}

			foreach (var member in file.Members())
			{
				member.ScriptFile = file;
			}

			return file;
		}

		private static List<Attribute> FindAttributes(SyntaxNode node, INode parent)
		{
			IEnumerable<AttributeListSyntax> attribList = null;
			//Namespaces technically aren't allowed to have attributes, but for our purposes we will allow them.
			//This requires us to find any attributes ourselves, as the SyntaxTree rejects them.
			if (node is NamespaceDeclarationSyntax)
			{
				//trick the SyntaxTree by making it look like an assembly-level attribute
				string trivia = node.GetLeadingTrivia().ToFullString().Replace("[", "[assembly: ");
				var root = CSharpSyntaxTree.ParseText(trivia).GetRoot();
				attribList = root.ChildNodes().OfType<AttributeListSyntax>();
			}
			else
			{
				attribList = node.ChildNodes().OfType<AttributeListSyntax>();
			}

			List<Attribute> result = new List<Attribute>();

			foreach (var alist in attribList)
			{
				foreach (var attrib in alist.Attributes)
				{
					Attribute a = new Attribute()
					{
						Name = attrib.Name.ToString(),
						//Declaration = alist.GetText().ToString().Replace("[assembly: ", "["),
						Parent = parent,
						Type = MemberType.Attribute
					};

					result.Add(a);

					//parent.Declaration = parent.Declaration.Replace(a.Declaration, "");

					var args = attrib.DescendantNodes().OfType<AttributeArgumentListSyntax>().FirstOrDefault();

					if (args is null)
						continue;

					foreach (var arg in args.Arguments)
					{
						a.Arguments.Add(arg.GetText().ToString());
					}
				}
			}



			return result;
		}

		private static Using ParseUsing(UsingDirectiveSyntax node)
		{
			var qualifiedName = node.ChildNodes().OfType<QualifiedNameSyntax>().FirstOrDefault();
			var identifier = node.ChildNodes().OfType<IdentifierNameSyntax>().FirstOrDefault();

			string name = "";
			if (qualifiedName != null)
			{
				name = qualifiedName.GetText().ToString();
			}
			else
			{
				name = identifier.GetText().ToString();
			}

			Using result = new Using()
			{
				Name = name,
				Type = MemberType.Using
			};

			//No reason for usings to ever be indented.
			result.Indent = "";
			result.OriginalText = node.GetText().ToString();

			return result;
		}

		private static Namespace ParseNamespace(NamespaceDeclarationSyntax node, INode parent)
		{
			Namespace result = new Namespace()
			{
				Name = node.Name.ToString(),
				Parent = parent,
				Type = MemberType.Namespace,
				Indent = "\t",
				OriginalText = node.GetText().ToString()
			};

			result.Modifiers.Add("namespace");
			result.Attributes = FindAttributes(node, result);

			foreach (var member in node.Members)
			{
				switch (member)
				{
					case NamespaceDeclarationSyntax x:
						result.AddNamespace(ParseNamespace(x, result));
						break;

					case ClassDeclarationSyntax x:
						result.AddClass(ParseClass(x, result, MemberType.Class));
						break;

					case InterfaceDeclarationSyntax x:
						result.AddInterface(ParseClass(x, result, MemberType.Interface));
						break;

					case StructDeclarationSyntax x:
						result.AddStruct(ParseClass(x, result, MemberType.Struct));
						break;

					case DelegateDeclarationSyntax x:
						result.AddDelegate(ParseDelegate(x, result, MemberType.Delegate));
						break;

					case EnumDeclarationSyntax x:
						result.AddEnum(ParseEnum(x, result));
						break;
				}
			}



			return result;
		}

		private static Class ParseClass(TypeDeclarationSyntax node, INode parent, MemberType type)
		{
			Class result = new Class()
			{
				Name = node.Identifier.Text.ToString(),
				Parent = parent,
				Type = type,
				Indent = "\t"
			};

			int StartPos = node.GetFirstToken().FullSpan.Start;
			int BodyStart = node.OpenBraceToken.FullSpan.Start;
			int BodyEnd = node.GetLastToken().FullSpan.End;

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			result.Modifiers.Add(type.ToString().ToLower());

			if (node.BaseList != null)
			{
				foreach (var Base in node.BaseList.Types)
				{
					result.BaseClasses.Add(Base.ToString());
				}
			}


			result.OriginalText = node.GetText().ToString();

			foreach (var member in node.Members)
			{
				switch (member)
				{
					case ClassDeclarationSyntax x:
						result.AddClass(ParseClass(x, result, MemberType.Class));
						break;

					case InterfaceDeclarationSyntax x:
						result.AddInterface(ParseClass(x, result, MemberType.Interface));
						break;

					case StructDeclarationSyntax x:
						result.AddStruct(ParseClass(x, result, MemberType.Struct));
						break;


					case MethodDeclarationSyntax x:
						result.AddMethod(ParseMethod(x, result, MemberType.Method));
						break;

					case ConstructorDeclarationSyntax x:
						result.AddConstructor(ParseMethod(x, result, MemberType.Constructor));
						break;

					case DestructorDeclarationSyntax x:
						result.AddFinalizer(ParseMethod(x, result, MemberType.Finalizer));
						break;

					case OperatorDeclarationSyntax x:
						result.AddOperator(ParseMethod(x, result, MemberType.Operator));
						break;


					case FieldDeclarationSyntax field:
						if (field.Modifiers.Any(x => x.IsKind(SyntaxKind.ConstKeyword)))
							result.AddConstant(ParseField(field, result, MemberType.Constant));
						else
							result.AddField(ParseField(field, result, MemberType.Field));
						break;

					case EventFieldDeclarationSyntax x:
						result.AddEvent(ParseEvent(x, result, MemberType.Event));
						break;

					case DelegateDeclarationSyntax x:
						result.AddDelegate(ParseDelegate(x, result, MemberType.Delegate));
						break;


					case PropertyDeclarationSyntax x:
						result.AddProperty(ParseProperty(x, result, MemberType.Property));
						break;

					case IndexerDeclarationSyntax x:
						result.AddIndexer(ParseProperty(x, result, MemberType.Indexer));
						break;


					case EnumDeclarationSyntax x:
						result.AddEnum(ParseEnum(x, result));
						break;
				}
			}

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Method ParseMethod(BaseMethodDeclarationSyntax node, INode parent, MemberType type)
		{
			string name = node.Name();
			if (type == MemberType.Finalizer)
			{
				name = $"~{name}";
			}
			else if (type == MemberType.Operator)
			{
				name = $"operator{(node as OperatorDeclarationSyntax).OperatorToken.ToString()}";
			}

			Method result = new Method()
			{
				Name = name,
				Parent = parent,
				Type = type,
				Indent = "\t"
			};

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			if (node is MethodDeclarationSyntax method)
			{
				if (method.ReturnType.Kind().ToString().Equals("TupleType"))
				{
					foreach (var ret in (method.ReturnType as TupleTypeSyntax).Elements)
					{
						result.ReturnTypes.Add(ret.ToString());
					}
				}
				else
				{
					result.ReturnTypes.Add(method.ReturnType.ToString());
				}
			}
			else if (node is OperatorDeclarationSyntax op)
			{
				if (op.ReturnType.Kind().ToString().Equals("TupleType"))
				{
					foreach (var ret in (op.ReturnType as TupleTypeSyntax).Elements)
					{
						result.ReturnTypes.Add(ret.ToString());
					}
				}
				else
				{
					result.ReturnTypes.Add(op.ReturnType.ToString());
				}
			}
			else if(node is ConstructorDeclarationSyntax con)
			{
				result.Initializer = con.Initializer?.ToString();
			}

			result.Parameters = node.ParameterList.Parameters.Select(x => new KeyValuePair<string, string>(x.Type.ToString(), x.Identifier.ToString())).ToList();

			if (node.Body != null)
			{
				result.Body = String.Join("\n", node.Body.Statements.Select(x => x.ToString()));
			}
			else if (node.ExpressionBody != null)
			{
				result.IsExpression = true;
				result.Body = node.ExpressionBody.Expression.ToString();
			}
			else
			{
				//partial function
				result.Body = ";";
			}
			result.OriginalText = node.ToString();

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Property ParseProperty(BasePropertyDeclarationSyntax node, INode parent, MemberType type)
		{
			string name = node.Name();
			string parameterType = "";
			string parameterName = "";
			if (type == MemberType.Indexer)
			{
				var param = node.ChildNodes().OfType<BracketedParameterListSyntax>().First();
				parameterType = param.Parameters.First().Type.ToString();
				parameterName = param.Parameters.First().Identifier.ToString();
				name = parameterType + "Indexer";
			}

			Property result = new Property()
			{
				Name = name,
				Parent = parent,
				Type = type,
				Indent = "\t",
				FieldType = node.Type.ToString(),
				IndexerType = parameterType,
				IndexerArg = parameterName
			};

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			if (node.ChildNodes().OfType<ArrowExpressionClauseSyntax>().Count() > 0)
			{
				result.IsExpression = true;
				var arrow = node.ChildNodes().OfType<ArrowExpressionClauseSyntax>().First();
				result.Body = arrow.Expression.ToString();
			}

			result.OriginalText = node.GetText().ToString();


			foreach (var accessor in node.DescendantNodes().OfType<AccessorDeclarationSyntax>())
			{
				if (accessor.Kind() == SyntaxKind.GetAccessorDeclaration)
				{
					if (accessor.ExpressionBody != null)
					{
						result.GetBody = accessor.ExpressionBody.ToString();
						result.GetExpression = true;
					}
					else if (accessor.ToString().Equals("get;"))
					{
						result.GetBody = "";
					}
					else
					{
						result.GetBody = node.DescendantNodes().OfType<BlockSyntax>().First().Statements.ToString();
					}
				}
				else
				{
					if (accessor.ExpressionBody != null)
					{
						result.SetBody = accessor.ExpressionBody.ToString();
						result.SetExpression = true;
					}
					else if (accessor.ToString().Contains("set;"))
					{
						result.SetBody = "";
					}
					else
					{
						result.SetBody = node.DescendantNodes().OfType<BlockSyntax>().First().Statements.ToString();
					}
				}
			}

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Delegate ParseDelegate(DelegateDeclarationSyntax node, INode parent, MemberType type)
		{
			Delegate result = new Delegate()
			{
				Name = node.Name(),
				Parent = parent,
				Type = type,
				Indent = "\t",
				OriginalText = node.GetText().ToString()
			};

			result.Attributes = FindAttributes(node, result);

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			result.ReturnType = node.ReturnType.ToString();
			var paramList = node.ParameterList?.Parameters;
			if (paramList != null)
			{
				foreach (var parameter in paramList)
				{
					result.Parameters.Add(new KeyValuePair<string, string>(parameter.Type.ToString(), parameter.Identifier.ToString()));
				}
			}

			return result;
		}

		private static Field ParseEvent(EventFieldDeclarationSyntax node, INode parent, MemberType type)
		{
			Field result = ParseBaseField(node, parent, type);

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			result.Modifiers.Add("event");
			var variable = node.ChildNodes().OfType<VariableDeclarationSyntax>().FirstOrDefault();
			result.VariableType = variable.Type.ToString();
			result.Name = variable.ChildNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault().Identifier.ToString();

			return result;
		}

		private static Field ParseField(FieldDeclarationSyntax node, INode parent, MemberType type)
		{
			Field result = ParseBaseField(node, parent, type);

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			if (type == MemberType.Constant)
			{
				result.Constant = true;
			}

			var variable = node.ChildNodes().OfType<VariableDeclarationSyntax>().FirstOrDefault();
			result.VariableType = variable.Type.ToString();
			result.Name = variable.ChildNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault().Identifier.ToString();

			return result;
		}

		private static Field ParseBaseField(MemberDeclarationSyntax node, INode parent, MemberType type)
		{
			Field result = new Field()
			{
				Name = node.Name(),
				Parent = parent,
				Type = type,
				Indent = "\t",
				OriginalText = node.GetText().ToString()
			};

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Enum ParseEnum(EnumDeclarationSyntax node, INode parent)
		{
			Enum result = new Enum()
			{
				Name = node.Name(),
				Parent = parent,
				Type = MemberType.Enum,
				Indent = "\t"
			};

			foreach (var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			result.OriginalText = node.GetText().ToString();

			result.Attributes = FindAttributes(node, result);

			foreach (var value in node.ChildNodes().OfType<EnumMemberDeclarationSyntax>())
			{
				string equals = value.EqualsValue?.ToString() ?? "";
				equals = Regex.Replace(equals, @" *= *", " = ");
				result.Values[value.Identifier.ToString()] = equals;
			}

			return result;
		}
	}
}
