﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;

namespace XGEF.Compiler
{
	public class EmptyMemberContainerTransformer : Transformer
	{
		protected override bool DoTransform(IEnumerable<ScriptFile> files, ScriptFile currentFile, INode member)
		{
			//This checks that no empty namespaces etc pollute the workspace
			switch (member.Type)
			{
				case MemberType.Namespace:
				case MemberType.Class:
				case MemberType.Struct:
				case MemberType.Interface:
					if (member.Children().Count() == 0)
						currentFile.RemoveMember(member);
					break;
				case MemberType.Enum:
					if(((Enum)member).Values.Count == 0)
						currentFile.RemoveMember(member);
					break;
			}

			return true;
		}
	}
}
