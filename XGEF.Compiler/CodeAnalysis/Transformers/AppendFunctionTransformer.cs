﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;
using System.Text.RegularExpressions;

namespace XGEF.Compiler
{
	public class AppendFunctionTransformer : Transformer<AppendFunctionAttribute>
	{
		protected override bool DoTransform(IEnumerable<ScriptFile> files, ScriptFile currentFile, INode member)
		{
			var tuple = FindMatchingAncestor(files, member);
			var oldFile = tuple.Item1;
			var replacing = tuple.Item2;
			if (replacing == null)
				throw new InvalidAncestorException($"{member.Type} {member.FullName} has no valid ancestor to append to!");

			if (replacing.IsExpression)
				throw new UnsupportedTransformationException($"{member.Type} {member.FullName} is an expression-bodied member!  It cannot be appended, only overwritten.");

			member.Body = $"{replacing.Body}\n{member.Body}";

			oldFile.RemoveMember(replacing);
			member.Attributes.Remove(member.GetAttribute(AttributeName));

			return true;
		}

		public AppendFunctionTransformer() : base()
		{
			ConflictingAttributes.Add("OverwriteAttribute");
			ConflictingAttributes.Add("ExtendAttribute");
			ConflictingAttributes.Add("AppendFunctionAttribute");
			ConflictingAttributes.Add("EncloseFunctionAttribute");
		}
	}
}
