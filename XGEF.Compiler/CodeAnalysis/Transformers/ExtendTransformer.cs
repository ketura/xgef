﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;
using System.Text.RegularExpressions;

namespace XGEF.Compiler
{
	public class ExtendTransformer : Transformer<ExtendAttribute>
	{
		protected override bool DoTransform(IEnumerable<ScriptFile> files, ScriptFile currentFile, INode member)
		{
			//ExplicitOverwriteTransformer at this point has already checked that unmarked members do not 
			// conflict with their ancestors.  We thus only need to check for the extension being valid 
			// and copying the relevant portion into the new and improved version of the file.
			var tuple = FindMatchingAncestor(files, member);
			var oldFile = tuple.Item1;
			var replacing = tuple.Item2;
			if (replacing == null)
				throw new InvalidAncestorException($"{member.Type} {member.FullName} has no valid ancestor to extend!");

			if (replacing.IsExpression)
				throw new UnsupportedTransformationException($"{member.Type} {member.FullName} is an expression-bodied member!  It cannot be extended, only overwritten.");

			foreach (var child in member.Children())
			{
				tuple = FindMatchingAncestor(files, child);
				var ancestor = tuple.Item2;
				if (ancestor != null && !member.HasAttribute(AttributeName))
					throw new InvalidAncestorException($"{member.Type} {member.FullName} has been marked [Extend], but it contains a member {child.Type} {child.Name} which attempts to hide a lower-priority mod member!");
			}

			string code = "";
			if (member is Enum e)
			{
				var replacingEnum = replacing as Enum;
				foreach(string name in e.Values.Keys)
				{
					if(replacingEnum.Values.ContainsKey(name))
					{
						throw new InvalidAttributeException($"Enum {member.FullName} contains a value {name} that conflicts with an existing value, but is marked as [Extend]!  Remove the value or change to [Overwrite].");
					}
				}

				code = replacing.Declaration + member.Surround($"{replacing.Body}, {member.Body}");
			}
			else
				code = replacing.Declaration + member.Surround($"{replacing.Body}\n{member.Body}");
			var tempMember = ScriptFile.AnalyzeFile(member.Filename, code).TopLevel.First().Value;

			string targetParent = member.Parent?.FullName;
			currentFile.RemoveMember(member);
			oldFile.RemoveMember(replacing);
			currentFile.InsertMember(tempMember, targetParent);

			return true;
		}

		public ExtendTransformer() : base()
		{
			ConflictingAttributes.Add("ExtendAttribute");
			ConflictingAttributes.Add("OverwriteAttribute");
			ConflictingAttributes.Add("AppendFunctionAttribute");
			ConflictingAttributes.Add("PrependFunctionAttribute");
			ConflictingAttributes.Add("EncloseFunctionAttribute");
		}
	}
}
