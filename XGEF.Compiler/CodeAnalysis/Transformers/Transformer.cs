﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Logging;

namespace XGEF.Compiler
{
	public abstract class Transformer
	{
		public string AttributeName { get { return Attribute?.ShortName; } }
		public XGEFAttribute Attribute { get; protected set; }
		public List<string> ConflictingAttributes { get; protected set; }
		public List<AttributeTargets> ValidOn { get; protected set; }

		public List<string> LoadedMods { get; set; }

		public virtual IEnumerable<ScriptFile> Transform(IEnumerable<ScriptFile> files)
		{
			foreach (var file in files.ToList())
			{
				var nodeList = file.Members().ToList();
				//reversing the depth-first member list is a crude but effective way of ensuring that children are processed before parents.
				nodeList.Reverse();
				foreach (var member in nodeList)
				{
					if (!member.HasAttribute(AttributeName))
						continue;

					Logger.Instance.Info($"Found {AttributeName} on {member.ToString()}.");
					ValidateAttributePlacement(member);

					if (!DoTransform(files, file, member))
						break;
				}
			}

			return files;
		}

		protected virtual bool DoTransform(IEnumerable<ScriptFile> files, ScriptFile currentFile, INode member) { return true; }

		public bool MatchesAttribute(string name)
		{
			if (Attribute == null)
				return false;

			return name.Equals(Attribute.ShortName) || name.Equals(Attribute.Name);
		}

		public virtual void ValidateAttributePlacement(INode node)
		{
			//Check that the attribute is on the supported kind of node
			var type = node.Type;
			if (!TargetLookup.ContainsKey(node.Type) || !ValidOn.Contains(TargetLookup[node.Type]))
			{
				throw new InvalidAttributeException($"{node.Type} {node.FullName} is not a valid target for attribute {AttributeName}!");
			}

			//If the transformer's attribute is listed as a conflict, this means we can't have more than 1.
			//Check to see if there is more than one in this case and if so, fail.
			if (ConflictingAttributes.Any(x => x.Equals(Attribute.Name)) &&
				node.Attributes.Where(x => MatchesAttribute(x.Name)).Count() > 1)
			{
				throw new DuplicateAttributeException($"{node.Type} {node.FullName} cannot have multiple {AttributeName} attributes!");
			}

			//check that other attributes on the node are kosher
			foreach (var attrib in node.Attributes)
			{
				if (MatchesAttribute(attrib.Name))
					continue;

				if (ConflictingAttributes.Contains(attrib.Name) || ConflictingAttributes.Contains(attrib.Name + "Attribute"))
				{
					throw new ConflictingAttributeException($"Attribute {attrib.Name} cannot be applied at the same time as attribute {AttributeName}!");
				}
			}

		}

		public virtual (ScriptFile, INode) FindMatchingAncestor(IEnumerable<ScriptFile> files, INode node)
		{
			INode result = null;
			ScriptFile fileResult = null;
			foreach (var file in files)
			{
				if (node.ScriptFile == file)
					break;

				result = file.FindExactMember(node);

				if (result != null)
				{
					fileResult = file;
					break;
				}
			}

			return (fileResult, result);
		}

		protected void RemoveAttribute(INode node)
		{
			node.Attributes.Remove(node.GetAttribute(AttributeName));
		}

		public List<MemberType> GetSupportedMemberTypes()
		{
			return TargetLookup.Where(x => ValidOn.Contains(x.Value)).Select(x => x.Key).ToList();
		}

		public List<AttributeTargets> GetSupportedAttributeTargets()
		{
			return TargetLookup.Where(x => ValidOn.Contains(x.Value)).Select(x => x.Value).ToList();
		}

		public static readonly Dictionary<MemberType, AttributeTargets> TargetLookup = new Dictionary<MemberType, AttributeTargets>()
		{
			[MemberType.Class] = AttributeTargets.Class,
			[MemberType.Interface] = AttributeTargets.Interface,
			[MemberType.Struct] = AttributeTargets.Struct,
			[MemberType.Enum] = AttributeTargets.Enum,
			[MemberType.Delegate] = AttributeTargets.Delegate,
			[MemberType.Method] = AttributeTargets.Method,
			[MemberType.Constructor] = AttributeTargets.Constructor,
			[MemberType.Finalizer] = AttributeTargets.Constructor,
			[MemberType.Property] = AttributeTargets.Property,
			[MemberType.Indexer] = AttributeTargets.Property,
			[MemberType.Event] = AttributeTargets.Event,
			[MemberType.Field] = AttributeTargets.Field,
			[MemberType.Constant] = AttributeTargets.Field,
			[MemberType.Namespace] = AttributeTargets.All,
			[MemberType.Operator] = AttributeTargets.Method,

			//[SyntaxKind.GenericName] = AttributeTargets.GenericParameter,
			//[SyntaxKind.Parameter] = AttributeTargets.Parameter,
			//[SyntaxKind.ReturnStatement] = AttributeTargets.ReturnValue
		};

		protected void Init()
		{
			ValidOn = new List<AttributeTargets>();

			if (Attribute == null)
				return;

			MemberInfo info = Attribute.GetType();
			var attribute = info.GetCustomAttributes<AttributeUsageAttribute>().First();

			foreach (AttributeTargets target in new AttributeTargets().GetValues())
			{
				if (attribute.ValidOn.HasFlag(target))
					ValidOn.Add(target);
			}
		}

		public Transformer()
		{
			LoadedMods = new List<string>();
		}
	}

	public abstract class Transformer<T> : Transformer
		where T : XGEFAttribute, new()
	{
		public T TAttribute { get { return Attribute as T; } }
		public Transformer() : base()
		{
			Attribute = new T();
			ConflictingAttributes = new List<string>();
			Init();
		}
	}
}
