﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Compiler
{
	public partial class Node
	{
		public virtual bool CompareSignature(INode node)
		{
			return this.Declaration.Equals(node.Declaration);
		}

		public virtual bool CompareName(INode node)
		{
			return this.Name.Equals(node.Name);
		}

		public virtual bool CompareFullName(INode node)
		{
			return this.FullName.Equals(node.FullName);
		}

		public virtual bool IsComparable(INode node)
		{
			return this.Type == node.Type;
		}

		public virtual bool CompareTo(INode node)
		{
			return IsCompatible(node) && CompareName(node) && CompareMembers(node);
		}

		public virtual bool IsCompatible(INode node)
		{
			return IsComparable(node) && CompareSignature(node);
		}

		public virtual bool CompareMembers(INode node)
		{
			if(IsComparable(node))
			{
				foreach(var child in this.Children())
				{
					var match = node.Where(x => x.IsCompatible(child) && x.CompareName(child)).FirstOrDefault();
					if (match == null || !match.CompareMembers(child))
					{
						return false; 
					}
				}

				return true;
			}
			else
			{
				return false;
			}
		}
	}

	//public partial class Property
	//{
	//	public override bool CompareSignature(INode node)
	//	{
	//		return this.
	//	}
	//}




}
