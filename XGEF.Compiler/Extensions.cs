﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Compiler
{
	public static class SyntaxNodeExtensions
	{
		public static string Name(this SyntaxNode node)
		{
			var children = node.ChildTokens().Where(x => x.IsKind(SyntaxKind.IdentifierToken));
			if (children.Count() == 0)
				return null;
			return children.First().ToString();
		}

		//public static string TypeName(this SyntaxNode node)
		//{
		//	var children = node.ChildTokens().Where(x => x.IsKind(SyntaxKind.typ));
		//	if (children.Count() == 0)
		//		return null;
		//	return node.ChildTokens().Where(x => x.IsKind(SyntaxKind.IdentifierToken)).First().ToString();
		//}
	}
}
