﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace XGEF.Compiler
{
	internal class DummyReferences
	{
		public static void PrimeReferences()
		{
			//The code warnings suggest making this a function, but such a function would then be optimized away.
			// The point is that it be kept in memory to force the listed types to be referenced.
#pragma warning disable IDE0039 // Use local function
			Action<Type> no_op = _ => { };
#pragma warning restore IDE0039 // Use local function
			//may need to add in a no-op lamda that takes a type, then loop through this and pass them all in
			Dictionary<string, Type> dummies = new Dictionary<string, Type>()
			{
				["Microsoft.CodeAnalysis.CSharp"] = typeof(Microsoft.CodeAnalysis.CSharp.Formatting.CSharpFormattingOptions)
				,
				["System.IO.FileSystem"] = typeof(global::System.IO.FileInfo)
				,
				["System.Reflection"] = typeof(global::System.Reflection.Assembly)
				,
				["System.Runtime"] = typeof(global::System.Runtime.GCSettings)
				,
				["System.Linq"] = typeof(global::System.Linq.Enumerable)
			};

			foreach (var type in dummies.Values)
			{
				no_op(type);
			}
		}

	}
}
