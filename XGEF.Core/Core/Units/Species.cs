﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Game.Stats;
using Newtonsoft.Json;

namespace XGEF.Units
{
	[JsonObject(MemberSerialization.OptOut)]
	public class Species : ContentEntity
	{
		[JsonProperty]
		public uint ID { get; set; }

		[JsonProperty]
		public string Name { get; protected set; }
		[JsonProperty]
		public string Description { get; protected set; }
		[JsonProperty]
		public string Notes { get; protected set; }

		public virtual void SetName(string name) { Name = name; }
		public virtual void SetDescription(string desc) { Description = desc; }
		public virtual void SetNotes(string notes) { Notes = notes; }

		//public Dictionary<string, Float01Stat> Types { get; protected set; }
		[JsonProperty]
		public Dictionary<string, LongStat> BaseStats { get; protected set; }
		//public Dictionary<string, Float01Stat> StatVariance { get; protected set; }
		//public Dictionary<Gender, Dictionary<string, Float02Stat>> Dimorphism { get; protected set; }
		//public string EvolvesFrom { get; set; }
		//public List<string> EvolvesTo { get; protected set; }
		//public List<string> Abilities { get; protected set; }
		//public List<string> Anatomy { get; protected set; }
		//public Float01Stat Diversity { get; protected set; }

		//public string OwningSystem { get; }

		//public Type EntityType { get { return this.GetType(); } }

		//public string Filename { get; protected set; }

		public void SetBaseStat(LongStat stat)
		{
			BaseStats[stat.Name] = stat;
		}

		public void AddBaseStat(string name, long amount)
		{
			SetBaseStat(CreateBaseStat(name, amount));
		}

		public Species() : this("UNNAMED_SPECIES", null, null) { }
		public Species(string name) : this(name, null, null) { }
		public Species(string name, string desc, string notes)
		{
			Name = name;
			Description = desc;
			Notes = notes;
			//Types = new Dictionary<string, Float01Stat>();
			BaseStats = new Dictionary<string, LongStat>();
			//StatVariance = new Dictionary<string, Float01Stat>();
			//Dimorphism = new Dictionary<Gender, Dictionary<string, Float02Stat>>
			//{
			//	[Gender.Male] = new Dictionary<string, Float02Stat>(),
			//	[Gender.Female] = new Dictionary<string, Float02Stat>()
			//};
			//EvolvesTo = new List<string>();
			//Abilities = new List<string>();
			//Anatomy = new List<string>();
			//Diversity = new Float01Stat("Diversity", 0.5f);
		}

		public static Float01Stat CreateTypeStat(string name, double value)
		{
			return new Float01Stat($"{name}Type", value, $"How much of the unit is pure {name} typing.",
				"This and other typing stats should all sum to 1.0.");
		}

		public static LongStat CreateBaseStat(string name, long value)
		{
			return new LongStat(name, value, 0, 1000);
		}
	}


}
