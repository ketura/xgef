﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using XGEF.Game.Stats;

namespace XGEF.Units
{
	[JsonObject]
	public class Unit : ContentEntity
	{
		[JsonProperty]
		public Species BaseSpecies { get; }

		[JsonProperty]
		public StatContainer<LongStat> Stats { get; protected set; }

		[JsonProperty]
		public string Name { get; protected set; }
		[JsonProperty]
		public string Description { get; protected set; }
		[JsonProperty]
		public string Notes { get; protected set; }

		public virtual void SetName(string name) { Name = name; }
		public virtual void SetDescription(string desc) { Description = desc; }
		public virtual void SetNotes(string notes) { Notes = notes; }

		public Unit(Species s)
		{
			BaseSpecies = s;

			Stats = new StatContainer<LongStat>();
			foreach(var stat in s.BaseStats.Values)
			{
				Stats[stat.Name] = new LongStat(stat.Name, (long)stat.Data);
			}
		}
	}
}
