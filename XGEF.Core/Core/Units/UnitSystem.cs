﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Units;


namespace XGEF.Units
{
	public abstract class ModUnitSystemBase : ModSystem<CoreUnitSystem>
	{
		public abstract Unit CreateUnitFromSpecies(Species s);
	}

	public class CoreUnitSystem : CoreSystem<ModUnitSystemBase>
	{
		public Species this[string id]
		{
			get { return SpeciesRegistry[id]; }
		}

		public bool AddSpecies(Species s)
		{
			if (!SpeciesRegistry.ContainsKey(s.Name))
			{
				SpeciesRegistry.Add(s.Name, s);
				return true;
			}
				
			SpeciesRegistry[s.Name] = s;
			return false;
		}

		public Unit FromSpecies(string name)
		{
			return FromSpecies(SpeciesRegistry[name]);
		}

		public Unit FromSpecies(uint id)
		{
			return FromSpecies(SpeciesRegistry.Values.FirstOrDefault(x => x.ID == id));
		}

		public Unit FromSpecies(Species s)
		{
			if (s == null)
				return null;

			return new Unit(s);
		}


		protected Dictionary<string, Species> SpeciesRegistry { get; set; }


		public CoreUnitSystem()
		{
			Name = "CoreUnitSystem";
			//this field should be superceded by the systems.cs file.
			ModdedSystemPath = "Engine/Units/UnitSystem.cs";
			ModdedSystemName = "UnitSystem";

			SpeciesRegistry = new Dictionary<string, Species>();
		}
	}


}
