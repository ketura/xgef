﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

using XGEF;

namespace XGEF.Game.Stats
{
	public class Float01Stat : NumericStat<double>
	{
		public override object Clone()
		{
			return new Float01Stat(this);
		}

		public static implicit operator Float01Stat(double num)
		{
			return new Float01Stat(num);
		}
		public static implicit operator double(Float01Stat stat)
		{
			return stat.Value;
		}

		[DefaultValue(1.0)]
		public override double MaxValue { get; protected set; }
		[DefaultValue(0.0)]
		public override double MinValue { get; protected set; }

		public Float01Stat(Float01Stat other) : this(other.Name, other.Value, other.Description, other.Notes) { }
		public Float01Stat() : this("UnnamedStat", 0, null, null) { }
		public Float01Stat(string name) : this(name, 0, null, null) { }
		public Float01Stat(double value) : this("UnnamedStat", value, null, null) { }
		public Float01Stat(string name, double value) : this(name, value, null, null) { }
		public Float01Stat(string name, double value, string desc, string notes) : base(name, value, 0, 1.0F, desc, notes)
		{
			MaxValue = 1.0;
			MinValue = 0.0;
		}
	}

	public class Float02Stat : NumericStat<double>
	{

		public override object Clone()
		{
			return new Float02Stat(this);
		}

		public static implicit operator Float02Stat(double num)
		{
			return new Float02Stat(num);
		}
		public static implicit operator double(Float02Stat stat)
		{
			return stat.Value;
		}
		public static implicit operator Float02Stat(float num)
		{
			return new Float02Stat((double)num);
		}
		public static implicit operator float(Float02Stat stat)
		{
			return (float)(stat.Value);
		}

		public Float02Stat(Float02Stat other) : this(other.Name, other.Value, other.Description, other.Notes) { }
		public Float02Stat() : this("UnnamedStat", 0, null, null) { }
		public Float02Stat(string name) : this(name, 0, null, null) { }
		public Float02Stat(double value) : this("UnnamedStat", value, null, null) { }
		public Float02Stat(string name, double value) : this(name, value, null, null) { }
		public Float02Stat(string name, double value, string desc, string notes) : base(name, value, 0, 2.0F, desc, notes) { }
	}


	#region Lazy Numeric Type stats

	public class DecimalStat : NumericStat<decimal>
	{
		public override object Clone()
		{
			return new DecimalStat(this);
		}

		public static implicit operator DecimalStat(decimal num)
		{
			return new DecimalStat(num);
		}
		public static implicit operator decimal(DecimalStat stat)
		{
			return stat.Value;
		}

		public DecimalStat(DecimalStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public DecimalStat() : this("UnnamedStat", 0, decimal.MinValue, decimal.MaxValue, null, null) { }
		public DecimalStat(string name) : this(name, 0, decimal.MinValue, decimal.MaxValue, null, null) { }
		public DecimalStat(decimal value) : this("UnnamedStat", value, decimal.MinValue, decimal.MaxValue, null, null) { }
		public DecimalStat(string name, decimal value) : this(name, value, decimal.MinValue, decimal.MaxValue, null, null) { }
		public DecimalStat(string name, decimal value, decimal min, decimal max) : this(name, value, decimal.MinValue, decimal.MaxValue, null, null) { }
		public DecimalStat(string name, decimal value, decimal min, decimal max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class DoubleStat : NumericStat<double>
	{
		public override object Clone()
		{
			return new DoubleStat(this);
		}

		public static implicit operator DoubleStat(double num)
		{
			return new DoubleStat(num);
		}
		public static implicit operator double(DoubleStat stat)
		{
			return stat.Value;
		}

		public DoubleStat(DoubleStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public DoubleStat() : this("UnnamedStat", 0, double.MinValue, double.MaxValue, null, null) { }
		public DoubleStat(string name) : this(name, 0, double.MinValue, double.MaxValue, null, null) { }
		public DoubleStat(double value) : this("UnnamedStat", value, double.MinValue, double.MaxValue, null, null) { }
		public DoubleStat(string name, double value) : this(name, value, double.MinValue, double.MaxValue, null, null) { }
		public DoubleStat(string name, double value, double min, double max) : this(name, value, double.MinValue, double.MaxValue, null, null) { }
		public DoubleStat(string name, double value, double min, double max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class FloatStat : NumericStat<float>
	{
		public override object Clone()
		{
			return new FloatStat(this);
		}

		public static implicit operator FloatStat(float num)
		{
			return new FloatStat(num);
		}
		public static implicit operator float(FloatStat stat)
		{
			return stat.Value;
		}

		public FloatStat(FloatStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public FloatStat() : this("UnnamedStat", 0, float.MinValue, float.MaxValue, null, null) { }
		public FloatStat(string name) : this(name, 0, float.MinValue, float.MaxValue, null, null) { }
		public FloatStat(float value) : this("UnnamedStat", value, float.MinValue, float.MaxValue, null, null) { }
		public FloatStat(string name, float value) : this(name, value, float.MinValue, float.MaxValue, null, null) { }
		public FloatStat(string name, float value, float min, float max) : this(name, value, float.MinValue, float.MaxValue, null, null) { }
		public FloatStat(string name, float value, float min, float max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class ByteStat : NumericStat<byte>
	{
		public override object Clone()
		{
			return new ByteStat(this);
		}

		public static implicit operator ByteStat(byte num)
		{
			return new ByteStat(num);
		}
		public static implicit operator byte(ByteStat stat)
		{
			return stat.Value;
		}

		public ByteStat(ByteStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public ByteStat() : this("UnnamedStat", 0, byte.MinValue, byte.MaxValue, null, null) { }
		public ByteStat(string name) : this(name, 0, byte.MinValue, byte.MaxValue, null, null) { }
		public ByteStat(byte value) : this("UnnamedStat", value, byte.MinValue, byte.MaxValue, null, null) { }
		public ByteStat(string name, byte value) : this(name, value, byte.MinValue, byte.MaxValue, null, null) { }
		public ByteStat(string name, byte value, byte min, byte max) : this(name, value, byte.MinValue, byte.MaxValue, null, null) { }
		public ByteStat(string name, byte value, byte min, byte max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class SByteStat : NumericStat<sbyte>
	{
		public override object Clone()
		{
			return new SByteStat(this);
		}

		public static implicit operator SByteStat(sbyte num)
		{
			return new SByteStat(num);
		}
		public static implicit operator sbyte(SByteStat stat)
		{
			return stat.Value;
		}

		public SByteStat(SByteStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public SByteStat() : this("UnnamedStat", 0, sbyte.MinValue, sbyte.MaxValue, null, null) { }
		public SByteStat(string name) : this(name, 0, sbyte.MinValue, sbyte.MaxValue, null, null) { }
		public SByteStat(sbyte value) : this("UnnamedStat", value, sbyte.MinValue, sbyte.MaxValue, null, null) { }
		public SByteStat(string name, sbyte value) : this(name, value, sbyte.MinValue, sbyte.MaxValue, null, null) { }
		public SByteStat(string name, sbyte value, sbyte min, sbyte max) : this(name, value, sbyte.MinValue, sbyte.MaxValue, null, null) { }
		public SByteStat(string name, sbyte value, sbyte min, sbyte max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class ShortStat : NumericStat<short>
	{
		public override object Clone()
		{
			return new ShortStat(this);
		}

		public static implicit operator ShortStat(short num)
		{
			return new ShortStat(num);
		}
		public static implicit operator short(ShortStat stat)
		{
			return stat.Value;
		}

		public ShortStat(ShortStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public ShortStat() : this("UnnamedStat", 0, short.MinValue, short.MaxValue, null, null) { }
		public ShortStat(string name) : this(name, 0, short.MinValue, short.MaxValue, null, null) { }
		public ShortStat(short value) : this("UnnamedStat", value, short.MinValue, short.MaxValue, null, null) { }
		public ShortStat(string name, short value) : this(name, value, short.MinValue, short.MaxValue, null, null) { }
		public ShortStat(string name, short value, short min, short max) : this(name, value, short.MinValue, short.MaxValue, null, null) { }
		public ShortStat(string name, short value, short min, short max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class UShortStat : NumericStat<ushort>
	{
		public override object Clone()
		{
			return new UShortStat(this);
		}

		public static implicit operator UShortStat(ushort num)
		{
			return new UShortStat(num);
		}
		public static implicit operator ushort(UShortStat stat)
		{
			return stat.Value;
		}

		public UShortStat(UShortStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public UShortStat() : this("UnnamedStat", 0, ushort.MinValue, ushort.MaxValue, null, null) { }
		public UShortStat(string name) : this(name, 0, ushort.MinValue, ushort.MaxValue, null, null) { }
		public UShortStat(ushort value) : this("UnnamedStat", value, ushort.MinValue, ushort.MaxValue, null, null) { }
		public UShortStat(string name, ushort value) : this(name, value, ushort.MinValue, ushort.MaxValue, null, null) { }
		public UShortStat(string name, ushort value, ushort min, ushort max) : this(name, value, ushort.MinValue, ushort.MaxValue, null, null) { }
		public UShortStat(string name, ushort value, ushort min, ushort max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class IntStat : NumericStat<int>
	{
		public override object Clone()
		{
			return new IntStat(this);
		}

		public static implicit operator IntStat(int num)
		{
			return new IntStat(num);
		}
		public static implicit operator int(IntStat stat)
		{
			return stat.Value;
		}

		public IntStat(IntStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public IntStat() : this("UnnamedStat", 0, int.MinValue, int.MaxValue, null, null) { }
		public IntStat(string name) : this(name, 0, int.MinValue, int.MaxValue, null, null) { }
		public IntStat(int value) : this("UnnamedStat", value, int.MinValue, int.MaxValue, null, null) { }
		public IntStat(string name, int value) : this(name, value, int.MinValue, int.MaxValue, null, null) { }
		public IntStat(string name, int value, int min, int max) : this(name, value, int.MinValue, int.MaxValue, null, null) { }
		public IntStat(string name, int value, int min, int max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class UIntStat : NumericStat<uint>
	{
		public override object Clone()
		{
			return new UIntStat(this);
		}

		public static implicit operator UIntStat(uint num)
		{
			return new UIntStat(num);
		}
		public static implicit operator uint(UIntStat stat)
		{
			return stat.Value;
		}

		public UIntStat(UIntStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public UIntStat() : this("UnnamedStat", 0, uint.MinValue, uint.MaxValue, null, null) { }
		public UIntStat(string name) : this(name, 0, uint.MinValue, uint.MaxValue, null, null) { }
		public UIntStat(uint value) : this("UnnamedStat", value, uint.MinValue, uint.MaxValue, null, null) { }
		public UIntStat(string name, uint value) : this(name, value, uint.MinValue, uint.MaxValue, null, null) { }
		public UIntStat(string name, uint value, uint min, uint max) : this(name, value, uint.MinValue, uint.MaxValue, null, null) { }
		public UIntStat(string name, uint value, uint min, uint max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class LongStat : NumericStat<long>
	{
		public override object Clone()
		{
			return new LongStat(this);
		}

		public static implicit operator LongStat(long num)
		{
			return new LongStat(num);
		}
		public static implicit operator long(LongStat stat)
		{
			return stat.Value;
		}

		public LongStat(LongStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public LongStat() : this("UnnamedStat", 0, long.MinValue, long.MaxValue, null, null) { }
		public LongStat(string name) : this(name, 0, long.MinValue, long.MaxValue, null, null) { }
		public LongStat(long value) : this("UnnamedStat", value, long.MinValue, long.MaxValue, null, null) { }
		public LongStat(string name, long value) : this(name, value, long.MinValue, long.MaxValue, null, null) { }
		public LongStat(string name, long value, long min, long max) : this(name, value, long.MinValue, long.MaxValue, null, null) { }
		public LongStat(string name, long value, long min, long max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}
	public class ULongStat : NumericStat<ulong>
	{
		public override object Clone()
		{
			return new ULongStat(this);
		}

		public static implicit operator ULongStat(ulong num)
		{
			return new ULongStat(num);
		}
		public static implicit operator ulong(ULongStat stat)
		{
			return stat.Value;
		}

		public ULongStat(ULongStat other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public ULongStat() : this("UnnamedStat", 0, ulong.MinValue, ulong.MaxValue, null, null) { }
		public ULongStat(string name) : this(name, 0, ulong.MinValue, ulong.MaxValue, null, null) { }
		public ULongStat(ulong value) : this("UnnamedStat", value, ulong.MinValue, ulong.MaxValue, null, null) { }
		public ULongStat(string name, ulong value) : this(name, value, ulong.MinValue, ulong.MaxValue, null, null) { }
		public ULongStat(string name, ulong value, ulong min, ulong max) : this(name, value, ulong.MinValue, ulong.MaxValue, null, null) { }
		public ULongStat(string name, ulong value, ulong min, ulong max, string desc, string notes) : base(name, value, min, max, desc, notes) { }
	}

	#endregion

}
