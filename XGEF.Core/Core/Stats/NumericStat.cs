﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Security.Permissions;

namespace XGEF.Game.Stats
{
	//this only exists so you can check if a variable is a NumericStat 
#pragma warning disable IDE1006
	public interface NumericStat { }
#pragma warning restore IDE1006

	public partial class NumericStat<T> : Stat<T>, NumericStat
		where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
	{
		[JsonIgnore]
		public virtual TypeCode TypeCode
		{
			get
			{
				return StatInfo.TypeCode;
			}
		}

		public NumericTypeInfo StatInfo { get; }

		public virtual T MaxValue { get; protected set; }
		public virtual T MinValue { get; protected set; }

		public override T Value
		{
			get { return RawValue; }
			set { Set(value); }
		}

		protected T RawValue { get; set; }

		public override void Set(object amount)
		{
			RawValue = ClampValue(amount);
		}

		protected virtual T ClampValue(T value)
		{
			return (value.CompareTo(MinValue) < 0) ? MinValue : ((value.CompareTo(MaxValue) > 0) ? MaxValue : value);
		}

		protected virtual T ClampValue(dynamic value)
		{
			dynamic temp = (T)value;
			return (temp.CompareTo(MinValue) < 0) ? MinValue : ((temp.CompareTo(MaxValue) > 0) ? MaxValue : temp);
		}

		public T ConvertValue(object value)
		{
			if (!NumericStatInfo.ContainsKey(value.GetType()))
				throw new ArgumentException($"Cannot convert value of non-primitive non-numeric type {value.GetType()}.");

			return (T)Convert.ChangeType(value, TypeCode);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public override int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			if (obj is IStat stat)
			{
				return RawValue.CompareTo(Convert.ChangeType(stat.Data, TypeCode));
			}

			return RawValue.CompareTo(Convert.ChangeType(obj, TypeCode));
		}

		public override int CompareTo(T other)
		{
			return RawValue.CompareTo(other);
		}

		public override int GetHashCode()
		{
			unchecked // Overflow is fine, just wrap
			{
				int hash = (int)2166136261;

				hash = hash * 16777619 + MinValue.GetHashCode();
				hash = hash * 16777619 + MaxValue.GetHashCode();
				hash = hash * 16777619 + Type.GetHashCode();
				hash = hash * 16777619 + Name.GetHashCode();
				hash = hash * 16777619 + (Description?.GetHashCode() ?? 0);
				hash = hash * 16777619 + (Notes?.GetHashCode() ?? 0);
				return hash;
			}
		}

		#region Casting


		public static implicit operator NumericStat<T>(T num)
		{
			return new NumericStat<T>() { Value = num };
		}

		public static implicit operator T(NumericStat<T> stat)
		{
			return stat.Value;
		}

		public static explicit operator decimal(NumericStat<T> stat)
		{
			return (decimal)(dynamic)stat.RawValue;
		}
		public static explicit operator double(NumericStat<T> stat)
		{
			return (double)(dynamic)stat.RawValue;
		}
		public static explicit operator float(NumericStat<T> stat)
		{
			return (float)(dynamic)stat.RawValue;
		}
		public static explicit operator long(NumericStat<T> stat)
		{
			return (long)(dynamic)stat.RawValue;
		}
		public static explicit operator ulong(NumericStat<T> stat)
		{
			return (ulong)(dynamic)stat.RawValue;
		}
		public static explicit operator int(NumericStat<T> stat)
		{
			return (int)(dynamic)stat.RawValue;
		}
		public static explicit operator uint(NumericStat<T> stat)
		{
			return (uint)(dynamic)stat.RawValue;
		}
		public static explicit operator short(NumericStat<T> stat)
		{
			return (short)(dynamic)stat.RawValue;
		}
		public static explicit operator ushort(NumericStat<T> stat)
		{
			return (ushort)(dynamic)stat.RawValue;
		}
		public static explicit operator byte(NumericStat<T> stat)
		{
			return (byte)(dynamic)stat.RawValue;
		}
		public static explicit operator sbyte(NumericStat<T> stat)
		{
			return (sbyte)(dynamic)stat.RawValue;
		}
		#endregion

		public override object Clone()
		{
			return new NumericStat<T>(this);
		}

		protected NumericStat(NumericStat<T> other) : this(other.Name, other.Value, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public NumericStat() : this("UnnamedStat", default, (T)NumericStatInfo[typeof(T)].MinValue, (T)NumericStatInfo[typeof(T)].MaxValue, null, null) { }
		public NumericStat(string name) : this(name, default, (T)NumericStatInfo[typeof(T)].MinValue, (T)NumericStatInfo[typeof(T)].MaxValue, null, null) { }
		public NumericStat(T value) : this("UnnamedStat", value, (T)NumericStatInfo[typeof(T)].MinValue, (T)NumericStatInfo[typeof(T)].MaxValue, null, null) { }
		public NumericStat(string name, T value) : this(name, value, (T)NumericStatInfo[typeof(T)].MinValue, (T)NumericStatInfo[typeof(T)].MaxValue, null, null) { }
		public NumericStat(string name, T value, T min, T max) : this(name, value, min, max, null, null) { }
		public NumericStat(string name, T value, T min, T max, string desc, string notes) : base(name, value, desc, notes)
		{

			if ((!typeof(T).IsPrimitive && typeof(T) != typeof(decimal))
				|| !NumericStatInfo.ContainsKey(typeof(T)))
				throw new ArgumentException($"Type {typeof(T).Name} is not a primitive numeric type.");

			StatInfo = NumericStatInfo[typeof(T)];

			MaxValue = max;
			MinValue = min;
			//Necessary here because the base version isn't called during the Stat constructor.
			Value = value;
		}
	}

	public partial class NumericStat<T>
	{
		public static readonly Dictionary<Type, NumericTypeInfo> NumericStatInfo = new Dictionary<Type, NumericTypeInfo>()
		{
			[typeof(decimal)] = new NumericTypeInfo() { Type = typeof(decimal), TypeCode = TypeCode.Decimal, Name = "decimal", SpecificName = "Decimal", MinValue = Decimal.MinValue, MaxValue = Decimal.MaxValue, LazyStatType = typeof(DecimalStat), Signed = true, Decimal = true, BitCount = 128 },
			[typeof(double)] = new NumericTypeInfo() { Type = typeof(double), TypeCode = TypeCode.Double, Name = "double", SpecificName = "Double", MinValue = Double.MinValue, MaxValue = Double.MaxValue, LazyStatType = typeof(DoubleStat), Signed = true, Decimal = true, BitCount = 64 },
			[typeof(float)] = new NumericTypeInfo() { Type = typeof(Single), TypeCode = TypeCode.Single, Name = "float", SpecificName = "Single", MinValue = Single.MinValue, MaxValue = Single.MaxValue, LazyStatType = typeof(FloatStat), Signed = true, Decimal = true, BitCount = 32 },
			[typeof(byte)] = new NumericTypeInfo() { Type = typeof(byte), TypeCode = TypeCode.Byte, Name = "byte", SpecificName = "Byte", MinValue = Byte.MinValue, MaxValue = Byte.MaxValue, LazyStatType = typeof(ByteStat), Signed = false, Decimal = false, BitCount = 8 },
			[typeof(sbyte)] = new NumericTypeInfo() { Type = typeof(sbyte), TypeCode = TypeCode.SByte, Name = "sbyte", SpecificName = "SByte", MinValue = SByte.MinValue, MaxValue = SByte.MaxValue, LazyStatType = typeof(SByteStat), Signed = true, Decimal = false, BitCount = 8 },
			[typeof(short)] = new NumericTypeInfo() { Type = typeof(Int16), TypeCode = TypeCode.Int16, Name = "short", SpecificName = "Int16", MinValue = Int16.MinValue, MaxValue = Int16.MaxValue, LazyStatType = typeof(ShortStat), Signed = true, Decimal = false, BitCount = 16 },
			[typeof(ushort)] = new NumericTypeInfo() { Type = typeof(UInt16), TypeCode = TypeCode.UInt16, Name = "ushort", SpecificName = "UInt16", MinValue = UInt16.MinValue, MaxValue = UInt16.MaxValue, LazyStatType = typeof(UShortStat), Signed = false, Decimal = false, BitCount = 16 },
			[typeof(int)] = new NumericTypeInfo() { Type = typeof(Int32), TypeCode = TypeCode.Int32, Name = "int", SpecificName = "Int32", MinValue = Int32.MinValue, MaxValue = Int32.MaxValue, LazyStatType = typeof(IntStat), Signed = true, Decimal = false, BitCount = 32 },
			[typeof(uint)] = new NumericTypeInfo() { Type = typeof(UInt32), TypeCode = TypeCode.UInt32, Name = "uint", SpecificName = "UInt32", MinValue = UInt32.MinValue, MaxValue = UInt32.MaxValue, LazyStatType = typeof(UIntStat), Signed = false, Decimal = false, BitCount = 32 },
			[typeof(long)] = new NumericTypeInfo() { Type = typeof(Int64), TypeCode = TypeCode.Int64, Name = "long", SpecificName = "Int64", MinValue = Int64.MinValue, MaxValue = Int64.MaxValue, LazyStatType = typeof(LongStat), Signed = true, Decimal = false, BitCount = 64 },
			[typeof(ulong)] = new NumericTypeInfo() { Type = typeof(UInt64), TypeCode = TypeCode.UInt64, Name = "ulong", SpecificName = "UInt64", MinValue = UInt64.MinValue, MaxValue = UInt64.MaxValue, LazyStatType = typeof(ULongStat), Signed = false, Decimal = false, BitCount = 64 }
		};
	}

	public struct NumericTypeInfo
	{
		public Type Type { get; set; }
		public TypeCode TypeCode { get; set; }
		public string Name { get; set; }
		public string SpecificName { get; set; }
		public dynamic MaxValue { get; set; }
		public dynamic MinValue { get; set; }
		public Type LazyStatType { get; set; }
		public bool Signed { get; set; }
		public bool Decimal { get; set; }
		public int BitCount { get; set; }
	}
}
