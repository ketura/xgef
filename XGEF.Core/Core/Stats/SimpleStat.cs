﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;

namespace XGEF.Game.Stats
{
	public abstract class SimpleStat<T> : Stat<T>
		where T : IComparable, IConvertible
	{
		public override T Value { get; set; }

		public override int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			if (obj is SimpleStat<T>)
			{
				var other = obj as SimpleStat<T>;
				return Value.CompareTo(other.Value);
			}

			if (obj is IStat)
			{
				return Value.CompareTo((T)(((IStat)obj).Data));
			}

			return Value.CompareTo((T)obj);
		}

		public override int CompareTo(T other)
		{
			return Value.CompareTo(other);
		}

		public override bool Equals(T other)
		{
			return Value.Equals(other);
		}

		public SimpleStat() : this("UnnamedStat", default(T), null, null) { }
		public SimpleStat(string name) : this(name, default(T), null, null) { }
		public SimpleStat(string name, T value) : this(name, value, null, null) { }
		public SimpleStat(string name, T value, string desc, string notes) : base(name, value, desc, notes) { }

	}

	public class StringStat : SimpleStat<string>
	{
		public override object Clone() { return new StringStat(this); }

		public StringStat(StringStat other) : this(other.Name, other.Value, other.Description, other.Notes) { }
		public StringStat() : this("UnnamedStat", null, null, null) { }
		public StringStat(string name) : this(name, null, null, null) { }
		public StringStat(string name, string value) : this(name, value, null, null) { }
		public StringStat(string name, string value, string desc, string notes) : base(name, value, desc, notes) { }
	}

	public class BoolStat : SimpleStat<bool>
	{
		public override object Clone() { return new BoolStat(this); }

		public BoolStat(BoolStat other) : this(other.Name, other.Value, other.Description, other.Notes) { }
		public BoolStat() : this("UnnamedStat", false, null, null) { }
		public BoolStat(string name) : this(name, false, null, null) { }
		public BoolStat(string name, bool value) : this(name, value, null, null) { }
		public BoolStat(string name, bool value, string desc, string notes) : base(name, value, desc, notes) { }
	}

}


