﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;

namespace XGEF.Game.Stats
{
	public abstract class ModStatSystemBase : ModSystem<CoreStatSystem>
	{
		public abstract ModRegistry<StatInfo> StatRegistry { get; }

		public abstract StatInfo GetStatInfoByAbbr(string abbr);
	}

	public class CoreStatSystem : CoreSystem<ModStatSystemBase>
	{
		public ModRegistry<StatInfo> StatRegistry { get { return ModSystem.StatRegistry; } }
		public StatInfo GetStatInfoByAbbr(string abbr) { return ModSystem.GetStatInfoByAbbr(abbr); }

		public CoreStatSystem()
		{
			Name = "CoreStatSystem";
			//this field should be superceded by the systems.cs file.
			ModdedSystemPath = "Engine/Stats/StatSystem.cs";
			ModdedSystemName = "StatSystem";
		}
	}

	public class StatInfo : INameInfo, IModEntity
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Notes { get; set; }
		public string Abbreviation { get; set; }
		public Type DataType { get; set; }
		public dynamic MinValue { get; set; }
		public dynamic MaxValue { get; set; }

		public ModRegistry<StringInfo> ExtraValues { get; protected set; }

		public void SetDescription(string desc) { Description = desc; }
		public void SetName(string name) { Name = name; }
		public void SetNotes(string notes) { Notes = notes; }

		public string ModName { get; set; }
		public Priority Priority { get; set; }

		public void SetPriority(Priority priority) { Priority = priority; }

		public StatInfo()
		{
			ExtraValues = new ModRegistry<StringInfo>();
			ModName = Constants.CoreModName;
			Priority = Priority.Normal;
		}
	}

}
