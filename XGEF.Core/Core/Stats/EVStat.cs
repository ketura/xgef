﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;

namespace XGEF.Game.Stats
{
	public class EVStat : NumericStat<ulong>
	{
		public static readonly ulong DefaultGrowthAmount = 1000;

		public ulong GrowthAmount { get; }

		public override ulong Value
		{
			get
			{
				var oldValue = Value;
				Value += GrowthAmount;
				return oldValue;
			}
			set { Value = value; }
		}

		public ulong Peek
		{
			get
			{
				var oldValue = Value;
				Value = oldValue;
				return oldValue;
			}
		}

		public override object Clone()
		{
			return new EVStat(Name, Value, GrowthAmount, MinValue, MaxValue, Description, Notes);
		}

		public EVStat(EVStat other) : this(other.Name, other.Value, other.GrowthAmount, other.MinValue, other.MaxValue, other.Description, other.Notes) { }
		public EVStat() : this("UnnamedStat", 0, DefaultGrowthAmount, ulong.MinValue, ulong.MaxValue, null, null) { }
		public EVStat(string name) : this(name, 0, DefaultGrowthAmount, ulong.MinValue, ulong.MaxValue, null, null) { }
		public EVStat(string name, ulong value) : this(name, value, DefaultGrowthAmount, ulong.MinValue, ulong.MaxValue, null, null) { }
		public EVStat(string name, ulong value, ulong growth) : this(name, value, growth, ulong.MinValue, ulong.MaxValue, null, null) { }
		public EVStat(string name, ulong value, ulong growth, ulong min, ulong max) : this(name, value, growth, min, max, null, null) { }
		public EVStat(string name, ulong value, ulong growth, ulong min, ulong max, string desc, string notes)
			: base(name, value, (ulong)(Math.Max(min, ulong.MinValue)), (ulong)(Math.Min(max, ulong.MaxValue)), desc, notes)
		{
			GrowthAmount = growth;
		}
	}

}


