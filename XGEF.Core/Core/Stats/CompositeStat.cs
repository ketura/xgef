﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;


namespace XGEF.Game.Stats
{
	public delegate U CombineCompositeStat<T, U>(Dictionary<string, T> stats, List<U> add, List<double> multiply)
		where T : IStat<U>;

	public class CompositeStat<T, U> : Stat<U>, IEquatable<CompositeStat<T, U>> where T : NumericStat<U>
		where U : struct, IComparable, IFormattable, IConvertible, IComparable<U>, IEquatable<U>
	{
		public override U Value
		{
			get { return CombineFunc(Stats, AdditionFactors, MultiplicationFactors); }
			set { }
		}

		public CombineCompositeStat<T, U> CombineFunc { get; protected set; }

		public Dictionary<string, T> Stats { get; protected set; }
		public List<U> AdditionFactors { get; protected set; }
		public List<double> MultiplicationFactors { get; protected set; }

		public static U DefaultCompositeCombine(Dictionary<string, T> stats, List<U> add, List<double> multiply)
		{
			dynamic baseStat = stats.FirstOrDefault(x => x.Key.Contains("BASE")).Value.Value;
			dynamic EV = stats.FirstOrDefault(x => x.Key.Contains("EV")).Value.Value;
			dynamic IV = stats.FirstOrDefault(x => x.Key.Contains("IV")).Value.Value;

			//(BaseStat + IV) * EV
			dynamic result = (baseStat + IV) * EV;

			foreach (U num in add)
				result += num;

			foreach (double num in multiply)
				result += num;

			return result;
		}

		public override int CompareTo(object obj)
		{
			return Value.CompareTo(obj);
		}

		public override int CompareTo(U other)
		{
			return Value.CompareTo(other);
		}

		public override bool Equals(object obj)
		{
			// STEP 1: Check for null
			if (obj == null)
			{
				return false;
			}

			// STEP 3: equivalent data types
			if (this.GetType() != obj.GetType())
			{
				return false;
			}
			return Equals((CompositeStat<T, U>)obj);
		}

		public bool Equals(CompositeStat<T, U> other)
		{
			if (other == null)
			{
				return false;
			}

			if (ReferenceEquals(this, other))
			{
				return true;
			}

			if (this.GetHashCode() != other.GetHashCode())
			{
				return false;
			}

			return EqualityComparer<U>.Default.Equals(Value, other.Value) &&
						 EqualityComparer<CombineCompositeStat<T, U>>.Default.Equals(CombineFunc, other.CombineFunc) &&
						 EqualityComparer<Dictionary<string, T>>.Default.Equals(Stats, other.Stats) &&
						 EqualityComparer<List<U>>.Default.Equals(AdditionFactors, other.AdditionFactors) &&
						 EqualityComparer<List<double>>.Default.Equals(MultiplicationFactors, other.MultiplicationFactors);
		}

		public override bool Equals(U other)
		{
			return Value.Equals(other);
		}

		public static bool operator ==(CompositeStat<T, U> stat1, CompositeStat<T, U> stat2)
		{
			return EqualityComparer<CompositeStat<T, U>>.Default.Equals(stat1, stat2);
		}

		public static bool operator !=(CompositeStat<T, U> stat1, CompositeStat<T, U> stat2)
		{
			return !(stat1 == stat2);
		}

		public override int GetHashCode()
		{
			var hashCode = 1042816794;
			hashCode = hashCode * -1521134295 + EqualityComparer<U>.Default.GetHashCode(Value);
			hashCode = hashCode * -1521134295 + EqualityComparer<CombineCompositeStat<T, U>>.Default.GetHashCode(CombineFunc);
			hashCode = hashCode * -1521134295 + EqualityComparer<Dictionary<string, T>>.Default.GetHashCode(Stats);
			hashCode = hashCode * -1521134295 + EqualityComparer<List<U>>.Default.GetHashCode(AdditionFactors);
			hashCode = hashCode * -1521134295 + EqualityComparer<List<double>>.Default.GetHashCode(MultiplicationFactors);
			return hashCode;
		}

		public override object Clone()
		{
			return new CompositeStat<T, U>(this);
		}

		public CompositeStat(CompositeStat<T, U> other) : this(other.Name, other.Stats.Values, other.CombineFunc, other.Description, other.Notes, other.AdditionFactors, other.MultiplicationFactors) { }
		public CompositeStat() : this("UnnamedStat", new List<T>(), DefaultCompositeCombine, null, null) { }
		public CompositeStat(string name, params T[] stats) : this(name, stats, DefaultCompositeCombine, null, null) { }
		public CompositeStat(string name, IEnumerable<T> stats, CombineCompositeStat<T, U> func, string desc, string notes, IEnumerable<U> adders = null, IEnumerable<double> multipliers = null) : base(name, default(T), desc, notes)
		{
			CombineFunc = func;

			Stats = new Dictionary<string, T>();
			foreach (var stat in stats)
				Stats.Add(stat.Name, stat);

			AdditionFactors = new List<U>();
			AdditionFactors.AddRange(adders ?? new List<U>());

			MultiplicationFactors = new List<double>();
			MultiplicationFactors.AddRange(multipliers ?? new List<double>());
		}
	}

}


