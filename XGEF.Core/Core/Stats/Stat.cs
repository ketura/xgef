﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

using System.Runtime.CompilerServices;

using Newtonsoft.Json;

using XGEF;

namespace XGEF.Game.Stats
{
	public interface IStat : INameInfo, IComparable
	{
		Type Type { get; }
		object Data { get; }
		void Set(object obj);
	}

	public interface IStat<T> : IStat
	{
		T Value { get; }
	}

	[JsonObject]
	public abstract class Stat<T> : IStat<T>, IComparable<T>, IEquatable<T>, ICloneable
		where T : IComparable
	{
		[JsonProperty]
		public virtual string Name { get; protected set; }
		[JsonProperty]
		public virtual string Description { get; protected set; }
		[JsonProperty]
		public virtual string Notes { get; protected set; }
		public virtual T Value { get; set; }
		[JsonIgnore]
		public virtual object Data { get { return Value; } }
		[JsonIgnore]
		public virtual Type Type { get { return typeof(T); } }

		public virtual void SetName(string name) { Name = name; }
		public virtual void SetDescription(string desc) { Description = desc; }
		public virtual void SetNotes(string notes) { Notes = notes; }

		public virtual void Set(object obj)
		{
			Value = (T)obj;
		}

		#region IComparable
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public virtual int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			if (obj is IStat stat)
			{
				return Value.CompareTo(stat.Data);
			}

			return Value.CompareTo(obj);
		}

		public virtual int CompareTo(T other)
		{
			return Value.CompareTo(other);
		}
		#endregion

		#region IEquatable
		public virtual bool Equals(T other)
		{
			return other.Equals(Value);
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			if (this.GetType() != obj.GetType())
			{
				return false;
			}
			return Equals((Stat<T>)obj);
		}

		public bool Equals(Stat<T> obj)
		{
			if (obj == null)
			{
				return false;
			}

			if (ReferenceEquals(this, obj))
			{
				return true;
			}

			if (this.GetHashCode() != obj.GetHashCode())
			{
				return false;
			}

			return this.Value.Equals(obj.Value) && this.Name.Equals(obj.Name) &&
							this.Description.Equals(obj.Description) && this.Notes.Equals(obj.Notes);

		}

		public override int GetHashCode()
		{
			unchecked // Overflow is fine, just wrap
			{
				int hash = (int)2166136261;

				hash = hash * 16777619 + Type.GetHashCode();
				hash = hash * 16777619 + Name.GetHashCode();
				hash = hash * 16777619 + (Description?.GetHashCode() ?? 0);
				hash = hash * 16777619 + (Notes?.GetHashCode() ?? 0);
				return hash;
			}
		}
		#endregion


		public abstract object Clone();

		public Stat() : this("UnnamedStat", default, null, null) { }
		public Stat(string name) : this(name, default, null, null) { }
		public Stat(string name, T value) : this(name, value, null, null) { }
		public Stat(string name, T value, string desc, string notes)
		{
			Name = name;
			Description = desc;
			Notes = notes;
			Value = value;
		}
	}

}


