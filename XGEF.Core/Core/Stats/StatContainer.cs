﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace XGEF.Game.Stats
{
	[JsonDictionary]
	public class StatContainer<V> : IDictionary<string, V>, INameInfo
		where V : IStat, new()
	{
		protected Dictionary<string, V> Stats { get; set; } = new Dictionary<string, V>();

		[JsonProperty]
		public virtual string Name { get; protected set; }
		[JsonProperty]
		public virtual string Description { get; protected set; }
		[JsonProperty]
		public virtual string Notes { get; protected set; }

		public virtual void SetName(string name) { Name = name; }
		public virtual void SetDescription(string desc) { Description = desc; }
		public virtual void SetNotes(string notes) { Notes = notes; }


		#region IDictionary
		public V this[string key] { get { return Stats[key]; } set { Stats[key] = value; } }
		public int Count { get { return Stats.Count; } }
		public bool IsReadOnly => false;
		public ICollection<string> Keys => Stats.Keys;
		public ICollection<V> Values => Stats.Values;

		

		public void Add(KeyValuePair<string, V> item) { Stats.Add(item.Key, item.Value); }
		public void Add(string key, V value) { Stats.Add(key, value); }
		public void Clear() { Stats.Clear(); }
		public bool Contains(KeyValuePair<string, V> item) { return Stats.Contains(item); }
		public bool ContainsKey(string key) { return Stats.ContainsKey(key); }
		public void CopyTo(KeyValuePair<string, V>[] array, int arrayIndex) { throw new NotImplementedException(); }
		public IEnumerator<KeyValuePair<string, V>> GetEnumerator() { return Stats.GetEnumerator(); }
		public bool Remove(KeyValuePair<string, V> item)
		{
			if (Stats[item.Key].Equals(item.Value))
				return Stats.Remove(item.Key);
			return false;
		}
		public bool Remove(string key) { return Stats.Remove(key); }
		public bool TryGetValue(string key, out V value) { return Stats.TryGetValue(key, out value); }
		IEnumerator IEnumerable.GetEnumerator() { return Stats.GetEnumerator(); }
		#endregion

		
		public StatContainer() : this(null, 0, null, new string[] { }) { }
		public StatContainer(object defaultValue, params string[] keys) : this(null, defaultValue, null, keys) { }
		public StatContainer(string itemDesc, params string[] keys) : this(null, 0, itemDesc, keys) { }
		public StatContainer(string name) : this(name, 0, null, new string[] { }) { }
		public StatContainer(string name, object defaultValue, params string[] keys) : this(name, defaultValue, null, keys) { }
		public StatContainer(string name, string itemDesc, params string[] keys) : this(name, 0, itemDesc, keys) { }
		public StatContainer(string name, object defaultValue, string itemDesc, params string[] keys)
		{
			if (defaultValue == null)
				defaultValue = 0;

			Name = name;

			Stats = new Dictionary<string, V>();

			foreach (string key in keys)
			{
				Stats[key] = new V();
				Stats[key].Set(defaultValue);
				Stats[key].SetName(key);
				string newdesc = itemDesc;
				if (itemDesc != null && itemDesc.Contains("{name}"))
					newdesc = itemDesc.Replace("{name}", key);
				Stats[key].SetDescription(newdesc);
			}
		}
	}
}
